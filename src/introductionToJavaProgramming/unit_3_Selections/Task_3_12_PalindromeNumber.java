package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.12, page 110
 * <p>
 * Write a program that prompts the user to enter a three-digit
 * integer and determines whether it is a palindrome number. A number is palindrome
 * if it reads the same from right to left and from left to right.
 */

public class Task_3_12_PalindromeNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a three digit integer: ");
        int threeDigitInteger = scanner.nextInt();

        int d1 = (int) ((threeDigitInteger / 100.0) % 10);
        int d3 = (int) ((threeDigitInteger / 1.0) % 10);

        if (d1 == d3){
            System.out.println(threeDigitInteger + " is a palindrome");
        }else{
            System.out.println(threeDigitInteger + " is not a palindrome");
        }
    }
}