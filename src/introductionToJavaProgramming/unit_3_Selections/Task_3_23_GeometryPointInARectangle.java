package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.23, page 113
 * <p>
 * Write a program that prompts the user to enter
 * a point (x, y) and checks whether the point is within the rectangle centered at
 * (0, 0) with width 10 and height 5. For example, (2, 2) is inside the rectangle and
 * (6, 4) is outside the rectangle, as shown in Figure 3.7b. (Hint: A point is in the
 * rectangle if its horizontal distance to (0, 0) is less than or equal to 10 / 2 and its
 * vertical distance to (0, 0) is less than or equal to 5.0 / 2. Test your program to
 * cover all cases.)
 */

public class Task_3_23_GeometryPointInARectangle {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter coordinate X: ");
        double coordinateX = scanner.nextDouble();
        System.out.println("Please enter coordinate Y: ");
        double coordinateY = scanner.nextDouble();

        double rectangleWidth = 10;
        double rectangleHeight = 5;

        if (coordinateX <= rectangleWidth / 2 && coordinateX <= rectangleWidth / -2
                && coordinateY <= rectangleHeight / 2 && coordinateY <= rectangleHeight / -2) {
            System.out.println("The point (" + coordinateX + "," + coordinateY + ") is in the rectangle.");
        } else {
            System.out.println("The point (" + coordinateX + "," + coordinateY + ") is not in the rectangle.");
        }
    }
}
