package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.11, page 110
 * <p>
 * (Find the number of days in a month) Write a program that prompts the user
 * to enter the month and year and displays the number of days in the month. For
 * example, if the user entered month 2 and year 2012, the program should display
 * that February 2012 had 29 days. If the user entered month 3 and year 2015, the
 * program should display that March 2015 had 31 days.
 */

public class Task_3_11_FindTheNumberOfDaysInAMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the month number: ");
        int monthNumber = scanner.nextInt();
        System.out.println("Please enter the year: ");
        int year = scanner.nextInt();

        int daysNumber;

        if (monthNumber == 1 || monthNumber == 3 || monthNumber == 5 || monthNumber == 7 || monthNumber == 8
                || monthNumber == 10 || monthNumber == 12) {
            daysNumber = 31;
        } else if (monthNumber == 2) {
            if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
                daysNumber = 29;
            } else {
                daysNumber = 28;
            }
        } else {
            daysNumber = 30;
        }

        String monthName = monthName(monthNumber);

        System.out.println(monthName + " has " + daysNumber + " days");

    }

    private static String monthName(int monthNumber){
        switch (monthNumber){
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "Invalid input = only accepts numbers 1 through 12";
        }
    }
}
