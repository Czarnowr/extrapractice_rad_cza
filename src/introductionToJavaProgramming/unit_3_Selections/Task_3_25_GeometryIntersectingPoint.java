package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.25, page 113
 * <p>
 * Two points on line 1 are given as (x1, y1) and (x2,
 * y2) and on line 2 as (x3, y3) and (x4, y4), as shown in Figure 3.8a–b.
 * <p>
 * The intersecting point of the two lines can be found by solving the following
 * linear equation:
 * <p>
 * (y1 - y2)x - (x1 - x2)y = (y1 - y2)x1 - (x1 - x2)y1
 * (y3 - y4)x - (x3 - x4)y = (y3 - y4)x3 - (x3 - x4)y3
 * <p>
 * This linear equation can be solved using Cramer’s rule (see Programming Exercise
 * 3.3). If the equation has no solutions, the two lines are parallel (Figure 3.8c).
 * <p>
 * Write a program that prompts the user to enter four points and displays the intersecting
 * point.
 */

public class Task_3_25_GeometryIntersectingPoint {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter: x1, y1, x2, y2, x3, y3, x4, y4: ");
        double x1 = scanner.nextDouble();
        double y1 = scanner.nextDouble();

        double x2 = scanner.nextDouble();
        double y2 = scanner.nextDouble();

        double x3 = scanner.nextDouble();
        double y3 = scanner.nextDouble();

        double x4 = scanner.nextDouble();
        double y4 = scanner.nextDouble();

        // Intersecting point:
        // (y1 - y2)x - (x1 - x2)y = (y1 - y2)x1 - (x1 - x2)y1
        // (y3 - y4)x - (x3 - x4)y = (y3 - y4)x3 - (x3 - x4)y3

        // Cramer's equation:
        // ax + by = e     x = (ed - bf) / (ad - bc)
        // cx + dy = f     y = (af - ec) / (ad - bc)

        double a = y1 - y2;
        double b = -1 * (x1 - x2);
        double c = y3 - y4;
        double d = -1 * (x3 - x4);
        double e = (y1 - y2) * x1 - (x1 - x2) * y1;
        double f = (y3 - y4) * x3 - (x3 - x4) * y3;

        if ((a * d) - (b * c) == 0) {
            System.out.println("The two lines are parallel");
        } else {
            double x = (e * d - b * f) / (a * d - b * c);
            double y = (a * f - e * c) / (a * d - b * c);
            System.out.println("The intersecting point is at (" + x + ", " + y + ")");
        }
    }
}
