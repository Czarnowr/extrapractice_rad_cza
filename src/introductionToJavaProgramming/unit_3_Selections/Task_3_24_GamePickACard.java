package introductionToJavaProgramming.unit_3_Selections;

/**
 * * Exercise 3.24, page 113
 * <p>
 * Write a program that simulates picking a card from a deck
 * of 52 cards. Your program should display the rank (Ace, 2, 3, 4, 5, 6, 7, 8, 9, 10,
 * Jack, Queen, King) and suit (Clubs, Diamonds, Hearts, Spades) of the card.
 */

public class Task_3_24_GamePickACard {
    public static void main(String[] args) {

        int deckSizeMin = 1;
        int deckSizeMax = 52;

        int randomCardNumber = (int) ((Math.random() * ((deckSizeMax - deckSizeMin) + 1)) + deckSizeMin);

        System.out.println("The card is: " + nameOfCard(randomCardNumber) + " of " + suitOfCard(randomCardNumber));

    }

    private static String nameOfCard(int cardNumber) {
        switch (cardNumber % 13) {
            case 0:
                return "Ace";
            case 1:
                return "2";
            case 2:
                return "3";
            case 3:
                return "4";
            case 4:
                return "5";
            case 5:
                return "6";
            case 6:
                return "7";
            case 7:
                return "8";
            case 8:
                return "9";
            case 9:
                return "10";
            case 10:
                return "Jack";
            case 11:
                return "Queen";
            case 12:
                return "King";
            default:
                return "Invalid Card Name Input";
        }
    }

    private static String suitOfCard(int cardNumber) {
        switch ((cardNumber % 13) / 4) {
            case 0:
                return "Clubs";
            case 1:
                return "Diamonds";
            case 2:
                return "Hearts";
            case 3:
                return "Spades";
            default:
                return "Invalid Card Suit Input";
        }
    }
}
