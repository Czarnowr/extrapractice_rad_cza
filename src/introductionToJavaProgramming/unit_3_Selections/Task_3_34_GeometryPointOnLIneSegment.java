package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * Exercise 3.34, page 118
 * <p>
 * Programming Exercise 3.32 shows how to test
 * whether a point is on an unbounded line. Revise Programming Exercise 3.32 to
 * test whether a point is on a line segment. Write a program that prompts the user
 * to enter the three points for p0, p1, and p2 and displays whether p2 is on the line
 * segment from p0 to p1.
 */

public class Task_3_34_GeometryPointOnLIneSegment {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter X for point 0: ");
        double point0coordinateX = scanner.nextDouble();
        System.out.println("Please enter Y for point 0: ");
        double point0coordinateY = scanner.nextDouble();

        System.out.println("Please enter X for point 1: ");
        double point1coordinateX = scanner.nextDouble();
        System.out.println("Please enter Y for point 1: ");
        double point1coordinateY = scanner.nextDouble();


        System.out.println("Please enter X for point 2: ");
        double point2coordinateX = scanner.nextDouble();
        System.out.println("Please enter Y for point 2: ");
        double point2coordinateY = scanner.nextDouble();

        double result = (point1coordinateX - point0coordinateX) * (point2coordinateY - point0coordinateY)
                - (point2coordinateX - point0coordinateX) * (point1coordinateY - point0coordinateY);

        boolean onLineSegment = false;

        if (result == 0) {
            onLineSegment = true;
        }

        boolean betweenPoints = false;

        if (point2coordinateX > point0coordinateX && point2coordinateY > point0coordinateY && point2coordinateX
                < point1coordinateX && point2coordinateY < point1coordinateY) {
            betweenPoints = true;
        }

        String modifier = "";

        if (!onLineSegment || !betweenPoints){
            modifier = "not ";
        }

        System.out.println("(" + point2coordinateX + ", " + point2coordinateY + ") is " + modifier + "on the line segment from ("
                + point0coordinateX + ", " + point0coordinateY + ") to (" + point1coordinateX + ", "
                + point1coordinateY + ")");
    }
}
