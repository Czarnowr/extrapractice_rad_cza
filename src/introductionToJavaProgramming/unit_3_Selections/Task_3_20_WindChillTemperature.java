package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.20, page 111
 * <p>
 * Programming Exercise 2.17 gives a formula
 * to compute the wind-chill temperature. The formula is valid for temperatures in
 * the range between −58ºF and 41ºF and wind speed greater than or equal to 2.
 * Write a program that prompts the user to enter a temperature and a wind speed.
 * The program displays the wind-chill temperature if the input is valid; otherwise,
 * it displays a message indicating whether the temperature and/or wind speed is
 * invalid.
 */

public class Task_3_20_WindChillTemperature {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter temperature between -58ºF and 41ºF: ");
        double outsideTemperature = scanner.nextDouble();
        System.out.println("Please enter wind speed higher than to or equal 2mph: ");
        double windSpeed = scanner.nextDouble();

        double windChillTemperature;

        if (outsideTemperature >= -58 && outsideTemperature <= 41 && windSpeed >= 2) {
            windChillTemperature = 35.74 + (0.6215 * outsideTemperature) - (35.75 * Math.pow(windSpeed, 0.16)) + (0.4275 * outsideTemperature * Math.pow(windSpeed, 0.16));
            System.out.println("Wind Chill Temperature equals: " + windChillTemperature);
        }else if ((outsideTemperature < -58 || outsideTemperature > 41) && windSpeed < 2) {
            System.out.println("The temperature and wind values are outside of the allowed range!");
        } else if (outsideTemperature < -58 || outsideTemperature > 41) {
            System.out.println("The temperature value is outside of the allowed range!");
        } else {
            System.out.println("The wind value is outside of the allowed range!");
        }
    }
}
