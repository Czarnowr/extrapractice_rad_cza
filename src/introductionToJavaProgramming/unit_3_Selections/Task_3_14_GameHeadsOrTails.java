package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.14, page 110
 * <p>
 * Write a program that lets the user guess whether the flip of
 * a coin results in heads or tails. The program randomly generates an integer 0 or 1,
 * which represents head or tail. The program prompts the user to enter a guess and
 * reports whether the guess is correct or incorrect.
 */

public class Task_3_14_GameHeadsOrTails {
    public static void main(String[] args) {

        double min = 0;
        double max = 1;

        int randomNumberBetween0and1 = (int) ((Math.random() * ((max - min) + 1)) + min);

        String result;

        switch (randomNumberBetween0and1) {
            case 0:
                result = "Heads";
                break;
            case 1:
                result = "Tails";
                break;
            default:
                result = "error";
                System.out.println("Incorrect input");
        }

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter '0' for 'Heads' or '1' for 'Tails'");
        int guess = scanner.nextInt();

        if (randomNumberBetween0and1 == guess) {
            System.out.println(result + ": Your guess was correct!");
        }else {
            System.out.println(result + ": Your guess was wrong!");
        }
    }
}


