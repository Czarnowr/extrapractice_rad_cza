package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.29, page 115
 * <p>
 * Write a program that prompts the user to enter the center
 * coordinates and radii of two circles and determines whether the second circle is
 * inside the first or overlaps with the first, as shown in Figure 3.10. (Hint: circle2 is
 * inside circle1 if the distance between the two centers  <= |r1 - r2| and circle2
 * overlaps circle1 if the distance between the two centers <= r1 + r2. Test your
 * program to cover all cases.)
 *
 * The formula for computing the distance is squareRoot of (x2 - x1)(x2 - x1) + (y2 - y1)*(y2 - y1).
 */

public class Task_3_29_GeometryTwoCircles {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter data for circle 1: centre x coordinate, centre y coordinate, radius");
        double coordinateX1 = scanner.nextDouble();
        double coordinateY1 = scanner.nextDouble();
        double radius1 = scanner.nextDouble();

        System.out.println("Enter data for circle 2: centre x coordinate, centre y coordinate, radius");
        double coordinateX2 = scanner.nextDouble();
        double coordinateY2 = scanner.nextDouble();
        double radius2 = scanner.nextDouble();

        double distanceBetweenCentres = Math.pow(Math.pow(coordinateX1 - coordinateX2, 2) + Math.pow(coordinateY1 - coordinateY2, 2), 0.5);

        if (distanceBetweenCentres <= radius1 - radius2){
            System.out.println("Circle 2 is inside circle 1");
        }else if (distanceBetweenCentres <= radius1 + radius2){
            System.out.println("Circle 2 overlaps circle 1");
        } else {
            System.out.println("Circle 2 is outside circle 1");
        }
    }
}
