package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.8, page 110
 * <p>
 * Write a program that prompts the user to enter three integers
 * and display the integers in non-decreasing order.
 */

public class Task_3_8_SortThreeIntegers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int integerOne = scanner.nextInt();
        int integerTwo = scanner.nextInt();
        int integerThree = scanner.nextInt();
        int temp;

        if (integerTwo > integerThree){
            temp = integerTwo;
            integerTwo = integerThree;
            integerThree = temp;
        }

        if (integerOne > integerTwo){
            temp = integerOne;
            integerOne = integerTwo;
            integerTwo = temp;
        }

        if (integerTwo > integerThree){
            temp = integerTwo;
            integerTwo = integerThree;
            integerThree = temp;
        }


        System.out.println(integerOne + " " + integerTwo + " " + integerThree);
    }
}
