package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * Exercise 3.31, page 116
 * <p>
 * Write a program that prompts the user to enter
 * the exchange rate from currency in U.S. dollars to Chinese RMB. Prompt the user
 * to enter 0 to convert from U.S. dollars to Chinese RMB and 1 to convert from
 * Chinese RMB and U.S. dollars. Prompt the user to enter the amount in U.S. dollars
 * or Chinese RMB to convert it to Chinese RMB or U.S. dollars, respectively.
 */

public class Task_3_31_FinancialsCurrencyExchange {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the USD to RMB exchange rate: ");
        double USDToRMB = scanner.nextDouble();
        System.out.println("Please enter transaction type: 0: USD -> RMB, 1: RMB -> USD");
        int transaction = scanner.nextInt();
        System.out.println("Please enter the amount of money to exchange: ");
        double amountOfMoney = scanner.nextDouble();

        double result;

        switch (transaction) {
            case 0:
                result = amountOfMoney * USDToRMB;
                System.out.println("$" + amountOfMoney + " is " + result + " yuan");
                break;
            case 1:
                result = amountOfMoney / USDToRMB;
                System.out.println(amountOfMoney + " yuan is " + result + " $");
                break;
            default:
                System.out.println("Invalid Input");
        }

    }
}
