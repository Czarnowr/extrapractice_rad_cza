package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.21, page 112
 * <p>
 * Zeller’s congruence is an algorithm developed by
 * Christian Zeller to calculate the day of the week. The formula is
 * <p>
 * h = (q + ((26 * (m - 1)) / 10) + k + (k/4) + (j/4) + (5*j)) / 7
 * <p>
 * where
 * ■ h is the day of the week (0: Saturday, 1: Sunday, 2: Monday, 3: Tuesday, 4:
 * Wednesday, 5: Thursday, 6: Friday).
 * ■ q is the day of the month.
 * ■ m is the month (3: March, 4: April, …, 12: December). January and February
 * are counted as months 13 and 14 of the previous year.
 * ■ j is the century (i.e.,
 * year
 * 100
 * ).
 * ■ k is the year of the century (i.e., year % 100).
 * <p>
 * Note that the division in the formula performs an integer division. Write a program
 * that prompts the user to enter a year, month, and day of the month, and
 * displays the name of the day of the week.
 */

public class Task_3_21_ScienceDayOfTheWeek {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter year (e.g., 2012): ");
        int year = scanner.nextInt();
        System.out.println("Enter month: ");
        int month = scanner.nextInt();
        System.out.println("Enter the day of the month (1 - 31): ");
        int day = scanner.nextInt();

        if (month == 1){
            month = 13;
            year--;
        }else if (month == 2){
            month = 14;
            year--;
        }

        int century = year / 100;
        int yearOfTheCentury = year % 100;

        int dayOfTheWeekNumber = (day + ((26 * (month + 1)) / 10) + yearOfTheCentury + (yearOfTheCentury / 4)
                + (century / 4) + (5 * century)) % 7;

        System.out.println("Day of the week is: " + getNameOfTheDay(dayOfTheWeekNumber));
    }

    private static String getNameOfTheDay(int dayOfTheWeekNumber) {
        switch (dayOfTheWeekNumber) {
            case 0:
                return "Saturday";
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            default:
                return "Invalid Input!";
        }
    }
}
