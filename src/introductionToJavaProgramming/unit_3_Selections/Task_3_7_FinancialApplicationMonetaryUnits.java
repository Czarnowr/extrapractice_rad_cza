package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.7, page 109
 * <p>
 * Modify Listing 2.10, ComputeChange
 * .java, to display the nonzero denominations only, using singular words for single
 * units such as 1 dollar and 1 penny, and plural words for more than one unit such
 * as 2 dollars and 3 pennies.
 */

public class Task_3_7_FinancialApplicationMonetaryUnits {
    public static void main(String[] args) {

        // variables
        double amountInDollarsAndCents;
        int amountInCents;
        int amountInDollars;
        int amountInQuarters;
        int amountInDimes;
        int amountInNickles;
        int amountInPennies;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again

        while (true) {
            System.out.println("Please enter the amount in dollars and cents: ");
            try {
                amountInDollarsAndCents = Double.parseDouble(scan.next());
                break;

            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }


        // mathematics (magic) happens here
        amountInCents = (int) (amountInDollarsAndCents * 100);

        amountInDollars = amountInCents / 100;
        amountInCents %= 100;

        amountInQuarters = amountInCents / 25;
        amountInCents %= 25;

        amountInDimes = amountInCents / 10;
        amountInCents %= 10;

        amountInNickles = amountInCents / 5;
        amountInCents %= 5;

        amountInPennies = amountInCents;

        // display results to console
        System.out.println("Your amount consists of: ");
        if (amountInDollars != 0) {
            System.out.println(amountInDollars + " dollar coin" + appendSForPlural(amountInDollars));
        }
        if (amountInQuarters != 0) {
            System.out.println(amountInQuarters + " quarter coin" + appendSForPlural(amountInQuarters));
        }
        if (amountInDimes != 0) {
            System.out.println(amountInDimes + " dime coin" + appendSForPlural(amountInDimes));
        }
        if (amountInNickles != 0) {
            System.out.println(amountInNickles + " nickle coin" + appendSForPlural(amountInNickles));
        }
        if (amountInPennies != 0) {
            System.out.println(amountInPennies + " penny coin" + appendSForPlural(amountInPennies));
        }
    }

    private static String appendSForPlural(int moneyAmount){
        if (moneyAmount > 1){
            return "s";
        }else{
            return "";
        }
    }
}
