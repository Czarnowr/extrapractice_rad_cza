package introductionToJavaProgramming.unit_3_Selections;

/**
 * * Exercise 3.16, page 111
 * <p>
 * Write a program that displays a random coordinate in a rectangle.
 * The rectangle is centered at (0, 0) with width 100 and height 200.
 */

public class Task_3_16_RandomPoint {
    public static void main(String[] args) {
        double rectangleTop = 100;
        double rectangleBottom = -100;

        int verticalCoordinate = (int) ((Math.random() * ((rectangleBottom - rectangleTop) + 1)) + rectangleTop);

        double rectangleLeft = -50;
        double rectangleRight = 50;

        int horizontalCoordinate = (int) ((Math.random() * ((rectangleRight - rectangleLeft) + 1)) + rectangleLeft);

        System.out.println("A random coordinate within a 50x100 rectangle (centered at 0,0): \n" +
                horizontalCoordinate + "," + verticalCoordinate);
    }
}
