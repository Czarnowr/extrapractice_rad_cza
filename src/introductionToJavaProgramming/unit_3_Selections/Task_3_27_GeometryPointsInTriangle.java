package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.27, page 114
 * <p>
 * Suppose a right triangle is placed in a plane as
 * shown below. The right-angle point is placed at (0, 0), and the other two points
 * are placed at (200, 0), and (0, 100). Write a program that prompts the user to enter
 * a point with x- and y-coordinates and determines whether the point is inside the
 * triangle.
 */

public class Task_3_27_GeometryPointsInTriangle {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter coordinate X: ");
        double userCoordinateX = scanner.nextDouble();
        System.out.println("Please enter coordinate Y: ");
        double userCoordinateY = scanner.nextDouble();

        double intersectionCoordinateX = (-userCoordinateX * (200 * 100)) / (-userCoordinateY * 200 - userCoordinateX * 100);
        double intersectionCoordinateY = (-userCoordinateY * (200 * 100)) / (-userCoordinateY * 200 - userCoordinateX * 100);

        if (userCoordinateX < intersectionCoordinateX || userCoordinateY < intersectionCoordinateY) {
            System.out.println("The point is within the triangle.");
        } else {
            System.out.println("The point is not within the triangle.");
        }
    }
}
