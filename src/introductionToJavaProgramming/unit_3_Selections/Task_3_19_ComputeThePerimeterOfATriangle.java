package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.19, page 111
 * <p>
 * Write a program that reads three edges for
 * a triangle and computes the perimeter if the input is valid. Otherwise, display that
 * the input is invalid. The input is valid if the sum of every pair of two edges is
 * greater than the remaining edge.
 */

public class Task_3_19_ComputeThePerimeterOfATriangle {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter length of side A: ");
        double sideA = scanner.nextDouble();
        System.out.println("Enter length of side B: ");
        double sideB = scanner.nextDouble();
        System.out.println("Enter length of side C: ");
        double sideC = scanner.nextDouble();

        if (sideA + sideB > sideC && sideA + sideC > sideB && sideB + sideC > sideA){
            System.out.println("The parameter of the given triangle is: " + (sideA + sideB + sideC) );
        }else{
            System.out.println("Invalid input - triangle cannot be created with those parameters!");
        }
    }
}