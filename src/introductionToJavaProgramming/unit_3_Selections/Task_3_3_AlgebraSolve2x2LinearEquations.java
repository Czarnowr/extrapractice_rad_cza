package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * Exercise 3.3, page 109
 *
 * A linear equation can be solved using
 * Cramer’s rule given in Programming Exercise 1.13. Write a program that prompts
 * the user to enter a, b, c, d, e, and f and displays the result. If ad - bc is 0, report
 * that “The equation has no solution.”
 */

public class Task_3_3_AlgebraSolve2x2LinearEquations {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter 'A'");
        double valueA = scan.nextDouble();
        System.out.println("Please enter 'B'");
        double valueB = scan.nextDouble();
        System.out.println("Please enter 'C'");
        double valueC = scan.nextDouble();
        System.out.println("Please enter 'D'");
        double valueD = scan.nextDouble();
        System.out.println("Please enter 'E'");
        double valueE = scan.nextDouble();
        System.out.println("Please enter 'F'");
        double valueF = scan.nextDouble();

        if (((valueA * valueD) - (valueB * valueC)) == 0) {
            System.out.println("The equation has no solution.");
        } else {
            double valueX = ((valueE * valueD - valueB * valueF)) / ((valueA * valueD) - (valueB * valueC));
            double valueY = ((valueA * valueF - valueE * valueC)) / ((valueA * valueD) - (valueB * valueC));
            System.out.println("X equals: " + valueX);
            System.out.println("Y equals: " + valueY);
        }
    }
}
