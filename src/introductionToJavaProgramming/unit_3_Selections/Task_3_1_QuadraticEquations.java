package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * Exercise 3.1, page 108
 */

public class Task_3_1_QuadraticEquations {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter the first number: ");
        double a = input.nextDouble();
        System.out.println("Please enter the second number: ");
        double b = input.nextDouble();
        System.out.println("Please enter the third number: ");
        double c = input.nextDouble();

        double discriminant = Math.pow(b, 2) - (4 * a * c);

        double firstRoot;
        double secondRoot;

        if (discriminant > 0) {
            firstRoot = (-b + Math.pow((Math.pow(b, 2) - (4 * a * c)), 0.5)) / (2 * a);
            secondRoot = (-b - Math.pow((Math.pow(b, 2) - (4 * a * c)), 0.5)) / (2 * a);
            System.out.println("The first root is: " + firstRoot + "\nThe second root is: " + secondRoot);
        } else if (discriminant == 0) {
            firstRoot = (-b + Math.pow((Math.pow(b, 2) - (4 * a * c)), 0.5)) / (2 * a);
            System.out.println("The first root is: " + firstRoot + "\nThere is no second root");
        } else {
            System.out.println("The equation has no real roots.");
        }
    }
}
