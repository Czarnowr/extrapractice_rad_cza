package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.30, page 116
 * <p>
 * Revise Programming Exercise 2.8 to display the hour using a
 * 12-hour clock.
 */

public class Task_3_30_CurrentTime {
    public static void main(String[] args) {

        /*
        Task 2.8, page 70

        (Current time) Listing 2.7, ShowCurrentTime.java, gives a program that displays the current time in GMT.
        Revise the program so that it prompts the user to enter the time zone offset to GMT and displays the time
        in the specified time zone.
         */

        //variables
        long currentTimeMilliSeconds = System.currentTimeMillis();
        int userTimeZoneOffset;
        long currentHoursUserTimeZone = 0;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for an integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter your Time Zone offset: ");
            try {
                userTimeZoneOffset = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // mathematics (magic) happens here
        long currentTimeSeconds = currentTimeMilliSeconds / 1000;
        long currentSecond = currentTimeSeconds % 60;
        long currentTimeMinutes = currentTimeSeconds / 60;
        long currentMinutes = currentTimeMinutes % 60;
        long currentTimeHours = currentTimeMinutes / 60;
        long currentHoursGMT = currentTimeHours % 60;

        if ((currentHoursGMT + userTimeZoneOffset) >= 0 && (currentHoursGMT + userTimeZoneOffset) <= 24) {
            currentHoursUserTimeZone = currentHoursGMT + userTimeZoneOffset;
        } else if ((currentHoursGMT + userTimeZoneOffset) > 24) {
            currentHoursUserTimeZone = ((currentHoursGMT + userTimeZoneOffset) % 24);
        } else {
            currentHoursUserTimeZone = 24 + (currentHoursGMT + userTimeZoneOffset);
        }

        // print results to console
        System.out.println("Current time GMT is: " + currentHoursGMT + ":" + currentMinutes + ":" + currentSecond + "\n");
        System.out.println("Current time GMT " + userTimeZoneOffset + " is: " + currentHoursUserTimeZone + ":" + currentMinutes + ":" + currentSecond);

    }
}
