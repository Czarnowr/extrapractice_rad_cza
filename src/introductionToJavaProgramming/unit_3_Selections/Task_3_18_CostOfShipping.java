package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.18, page 111
 * <p>
 * A shipping company uses the following function to calculate
 * the cost (in dollars) of shipping based on the weight of the package (in
 * pounds).
 * <p>
 * c(w) =
 * <p>
 * 3.5,  if  0  <  w  <= 1
 * 5.5,  if  1  <  w  <= 3
 * 8.5,  if  3  <  w  <= 10
 * 10.5, if  10 <  w  <= 20
 * <p>
 * Write a program that prompts the user to enter the weight of the package and
 * display the shipping cost. If the weight is greater than 50, display a message “the
 * package cannot be shipped.”
 */

public class Task_3_18_CostOfShipping {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the weight of the parcel: ");
        double weightOfParcel = scanner.nextInt();

        double costOfShipping = 0;

        if (weightOfParcel > 0 && weightOfParcel <= 1) {
            costOfShipping = 3.5;
        } else if (weightOfParcel > 1 && weightOfParcel <= 3) {
            costOfShipping = 5.5;
        } else if (weightOfParcel > 3 && weightOfParcel <= 10) {
            costOfShipping = 8.5;
        } else if (weightOfParcel > 10 && weightOfParcel <= 20) {
            costOfShipping = 10.5;
        } else {
            System.out.println("The package cannot be shipped");
        }

        if (costOfShipping > 0) {
            System.out.println("The cost of shipping is " + costOfShipping + " Dollars");
        }
    }
}
