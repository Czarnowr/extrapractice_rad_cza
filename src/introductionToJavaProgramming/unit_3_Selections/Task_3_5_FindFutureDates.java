package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.5, page 109
 * <p>
 * Write a program that prompts the user to enter an integer for
 * today’s day of the week (Sunday is 0, Monday is 1, …, and Saturday is 6). Also
 * prompt the user to enter the number of days after today for a future day and display
 * the future day of the week.
 */

public class Task_3_5_FindFutureDates {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the day number:\n0: Sunday\n1: Monday\n2: Tuesday\n3: Wednesday\n4: Thursday" +
                "\n5: Friday\n6: Saturday");
        int inputForToday = scanner.nextInt();

        System.out.println("Please enter the number of days elapsed from today:");
        int inputForDayInTheFuture = scanner.nextInt();
        int relativeDifference = inputForDayInTheFuture % 7;

        System.out.println("Today is " + checkDayOfTheWeek(inputForToday) + " and the future day is " + checkDayOfTheWeek((inputForToday + relativeDifference)%7));

    }

    private static String checkDayOfTheWeek(int dayNumber) {
        switch (dayNumber) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            default:
                return "Input not valid";
        }
    }

}
