package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * Exercise 3.32, page 117
 * <p>
 * Given a directed line from point p0(x0, y0) to p1(x1,
 * y1), you can use the following condition to decide whether a point p2(x2, y2) is
 * on the left of the line, on the right, or on the same line (see Figure 3.11):
 * <p>
 * (x1 - x0)*(y2 - y0) - (x2 - x0)*(y1 - y0)
 * <p>
 * > 0 p2 is on the left side of the line
 * = 0 p2 is on the same line
 * < 0 p2 is on the right side of the line
 * <p>
 * Write a program that prompts the user to enter the three points for p0, p1, and p2
 * and displays whether p2 is on the left of the line from p0 to p1, on the right, or on
 * the same line.
 */

public class Task_3_32_GeometryPointPosition {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter X for point 0: ");
        double point0coordinateX = scanner.nextDouble();
        System.out.println("Please enter Y for point 0: ");
        double point0coordinateY = scanner.nextDouble();

        System.out.println("Please enter X for point 1: ");
        double point1coordinateX = scanner.nextDouble();
        System.out.println("Please enter Y for point 1: ");
        double point1coordinateY = scanner.nextDouble();


        System.out.println("Please enter X for point 2: ");
        double point2coordinateX = scanner.nextDouble();
        System.out.println("Please enter Y for point 2: ");
        double point2coordinateY = scanner.nextDouble();

        double result = (point1coordinateX - point0coordinateX) * (point2coordinateY - point0coordinateY)
                - (point2coordinateX - point0coordinateX) * (point1coordinateY - point0coordinateY);

        String position;

        if (result > 0) {
            position = "on the left side of ";
        } else if (result == 0) {
            position = "on ";
        } else {
            position = "on the right side of ";
        }

        System.out.println("Point (" + point2coordinateX + ", " + point2coordinateY + ") is " + position + "the " +
                "line from point (" + point0coordinateX + ", " + point0coordinateY + ") to point ("
                + point1coordinateX + ", " + point1coordinateY + ")");

    }
}
