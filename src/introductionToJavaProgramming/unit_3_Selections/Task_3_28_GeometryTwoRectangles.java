package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.28, page 115
 * <p>
 * Write a program that prompts the user to enter the
 * center x-, y-coordinates, width, and height of two rectangles and determines
 * whether the second rectangle is inside the first or overlaps with the first, as shown
 * in Figure 3.9. Test your program to cover all cases.
 */

public class Task_3_28_GeometryTwoRectangles {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter data for rectangle 1: centre x coordinate, centre y coordinate, width, height");
        double coordinateX1 = scanner.nextDouble();
        double coordinateY1 = scanner.nextDouble();
        double height1 = scanner.nextDouble();
        double width1 = scanner.nextDouble();

        System.out.println("Enter data for rectangle 2: centre x coordinate, centre y coordinate, width, height");
        double coordinateX2 = scanner.nextDouble();
        double coordinateY2 = scanner.nextDouble();
        double height2 = scanner.nextDouble();
        double width2 = scanner.nextDouble();

        double right1 = coordinateX1 + width1 / 2.0;
        double left1 = coordinateX1 - width1 / 2.0;
        double top1 = coordinateY1 + height1 / 2.0;
        double bottom1 = coordinateY1 - height1 / 2.0;
        double right2 = coordinateX2 + width2 / 2.0;
        double left2 = coordinateX2 - width2 / 2.0;
        double top2 = coordinateY2 + height2 / 2.0;
        double bottom2 = coordinateY2 - height2 / 2.0;

        if (right2 <= right1 && left2 >= left1 && top2 <= top1 && bottom2 >= bottom1) {
            System.out.println("r2 is inside r1.");
        } else if (right2 <= left1 || left2 > right1 || bottom2 >= top1 || top2 <= bottom1) {
            System.out.println("r2 does not overlap r1");
        } else {
            System.out.println("r2 overlaps r1");
        }
    }
}
