package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.22, page 112
 * <p>
 * Write a program that prompts the user to enter a
 * point (x, y) and checks whether the point is within the circle centered at (0, 0)
 * with radius 10. For example, (4, 5) is inside the circle and (9, 9) is outside the
 * circle, as shown in Figure 3.7a.
 *
 * (Hint: A point is in the circle if its distance to (0, 0) is less than or equal to 10.
 *
 * The formula for computing the distance is squareRoot of (x2 - x1)(x2 - x1) + (y2 - y1)*(y2 - y1). Test your
 * program to cover all cases.)
 */

public class Task_3_22_GeometryPointInACircle {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter coordinate X: ");
        double coordinateX = scanner.nextDouble();
        System.out.println("Please enter coordinate Y: ");
        double coordinateY = scanner.nextDouble();

        double circleRadius = 10;
        double distanceFromCenter = Math.pow((Math.pow(coordinateX - 0, 2) + Math.pow(coordinateY - 0, 2)), 0.5);

        if (distanceFromCenter <= circleRadius){
            System.out.println("The point (" + coordinateX + "," + coordinateY + ") is in the circle.");
        }else{
            System.out.println("The point (" + coordinateX + "," + coordinateY + ") is not in the circle.");
        }
    }
}
