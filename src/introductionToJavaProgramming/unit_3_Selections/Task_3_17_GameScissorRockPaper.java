package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.17, page 111
 * <p>
 * Write a program that plays the popular scissor-rockpaper
 * game. (A scissor can cut a paper, a rock can knock a scissor, and a paper can
 * wrap a rock.) The program randomly generates a number 0, 1, or 2 representing
 * scissor, rock, and paper. The program prompts the user to enter a number 0, 1, or
 * 2 and displays a message indicating whether the user or the computer wins, loses,
 * or draws.
 */

public class Task_3_17_GameScissorRockPaper {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose one: scissor (0), rock (1), paper (2):");
        int playersChoice = scanner.nextInt();

        double min = 0;
        double max = 2;

        int computersChoice = (int) ((Math.random() * ((max - min) + 1)) + min);

        switch (playersChoice) {
            case 0:
                if (computersChoice == 0) {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + " too. It is a draw!");

                } else if (computersChoice == 1) {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + ". You lose!");
                } else {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + ". You win!");
                }
                break;
            case 1:
                if (computersChoice == 1) {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + " too. It is a draw!");
                } else if (computersChoice == 2) {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + ". You lose!");
                } else {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + ". You win!");
                }
                break;
            case 2:
                if (computersChoice == 2) {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + " too. It is a draw!");
                } else if (computersChoice == 0) {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + ". You lose!");
                } else {
                    System.out.println("The computer is: " + choiceAsName(computersChoice) + ". You are " +
                            choiceAsName(playersChoice) + ". You win!");
                }
                break;
            default:
                System.out.println("Invalid input");
        }
    }

    private static String choiceAsName(int computerChoice) {
        switch (computerChoice) {
            case 0:
                return "scissor";
            case 1:
                return "rock";
            case 2:
                return "paper";
            default:
                return "invalid input";
        }
    }

}
