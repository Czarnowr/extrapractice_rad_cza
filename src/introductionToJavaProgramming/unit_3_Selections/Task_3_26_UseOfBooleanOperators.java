package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.26, page 114
 * <p>
 * Write a program that prompts the user to enter
 * an integer and determines whether it is divisible by 5 and 6, whether it is divisible
 * by 5 or 6, and whether it is divisible by 5 or 6, but not both.
 */

public class Task_3_26_UseOfBooleanOperators {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a number: ");
        int userInput = scanner.nextInt();

        System.out.println(userInput + " is divisible by 5 and 6: " + (userInput % 5 == 0 && userInput % 6 == 0));
        System.out.println(userInput + " is divisible by 5 or 6: " + (userInput % 5 == 0 || userInput % 6 == 0));
        System.out.println(userInput + " is divisible by 5 or 6, but not both: " + (userInput % 5 != 0 || userInput % 6 != 0));
        System.out.println(userInput + " is not divisible by 5 and 6: " + (userInput % 5 != 0 && userInput % 6 != 0));
    }
}
