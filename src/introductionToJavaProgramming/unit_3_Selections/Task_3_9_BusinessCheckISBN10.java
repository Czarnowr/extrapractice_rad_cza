package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * * Exercise 3.9, page 110
 * <p>
 * An ISBN-10 (International Standard Book Number)
 * consists of 10 digits: d1d2d3d4d5d6d7d8d9d10. The last digit, d10, is a checksum,
 * which is calculated from the other nine digits using the following formula:
 * (d1 * 1 + d2 * 2 + d3 * 3 + d4 * 4 + d5 * 5 +
 * d6 * 6 + d7 * 7 + d8 * 8 + d9 * 9) % 11
 * If the checksum is 10, the last digit is denoted as X according to the ISBN-10
 * convention. Write a program that prompts the user to enter the first 9 digits and
 * displays the 10-digit ISBN (including leading zeros). Your program should read
 * the input as an integer.
 */

public class Task_3_9_BusinessCheckISBN10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the first 9 digits of an ISBN integer: ");
        int firstNineDigits = scanner.nextInt();

        int d1 = (int) ((firstNineDigits / 100000000.0) % 10);
        int d2 = (int) ((firstNineDigits / 10000000.0) % 10);
        int d3 = (int) ((firstNineDigits / 1000000.0) % 10);
        int d4 = (int) ((firstNineDigits / 100000.0) % 10);
        int d5 = (int) ((firstNineDigits / 10000.0) % 10);
        int d6 = (int) ((firstNineDigits / 1000.0) % 10);
        int d7 = (int) ((firstNineDigits / 100.0) % 10);
        int d8 = (int) ((firstNineDigits / 10.0) % 10);
        int d9 = (int) ((firstNineDigits / 1.0) % 10);

        int d10temp = (d1 + d2 * 2 + d3 * 3 + d4 * 4 + d5 * 5 + d6 * 6 + d7 * 7 + d8 * 8 + d9 * 9) % 11;
        String d10;

        if (d10temp == 10){
            d10 = "X";
        }else{
            d10 = String.valueOf(d10temp);
        }

        System.out.println("The ISBN-10 number is " + d1 + "" + d2 + "" + d3 + "" + d4 + "" + d5 + "" + d6 + "" + d7 + "" + d8 + "" + d9 + "" + d10);

    }
}
