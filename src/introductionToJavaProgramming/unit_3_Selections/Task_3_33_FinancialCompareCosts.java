package introductionToJavaProgramming.unit_3_Selections;

import java.util.Scanner;

/**
 * Exercise 3.33, page 117
 * <p>
 * Suppose you shop for rice in two different packages.
 * You would like to write a program to compare the cost. The program prompts the
 * user to enter the weight and price of the each package and displays the one with
 * the better price.
 */

public class Task_3_33_FinancialCompareCosts {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the weight of package 1: ");
        double package1weight = scanner.nextDouble();
        System.out.println("Please enter the price of package 1: ");
        double package1price = scanner.nextDouble();

        System.out.println("Please enter the weight of package 2: ");
        double package2weight = scanner.nextDouble();
        System.out.println("Please enter the price of package 2: ");
        double package2price = scanner.nextDouble();

        double priceRatioOfPackage1 = package1price / package1weight;
        double priceRatioOfPackage2 = package2price / package2weight;

        String relationship;

        if (priceRatioOfPackage1 > priceRatioOfPackage2){
            relationship = "higher than";
        }else if (priceRatioOfPackage1 == priceRatioOfPackage2){
            relationship = "equal to";
        }else{
            relationship = "lower than";
        }

        System.out.println("The price of package 1 is " + relationship + " the price of package 2.");
    }
}
