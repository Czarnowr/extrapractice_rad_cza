package introductionToJavaProgramming.unit_4_MathematicalFunctionsCharactersAndStrings;

/**
 * Exercise 4.1, page 150
 * <p>
 * Write a program that prompts the user to enter
 * the length from the center of a pentagon to a vertex and computes the area of the
 * pentagon, as shown in the following figure.
 *
 * The formula for computing the area of a pentagon is:
 *
 *      Area = (5 * s * s) / (4 * tan(pi/5))
 *
 * where s is the length of a side. The side can be computed using the formula:
 *
 *      s = 2 * r * sin(pi/5)
 *
 * where r is the length from the center of a pentagon to a vertex. Round up two digits
 * after the decimal point.
 */

public class Task_4_1_GeometryAreaOfPentagon {
}
