package introductionToJavaProgramming.unit_11_InheritenceAndPolymorphism;

import java.util.Date;

public class Task_11_0_1_SimpleGoemetricObject {
    private String color = "white";
    private boolean filled;
    private java.util.Date dateCreated;

    /** Construct a default geometric object*/
    public Task_11_0_1_SimpleGoemetricObject(){
        dateCreated = new java.util.Date();
    }

    /** Construct a geometric object with the specified color and filled value*/
    public Task_11_0_1_SimpleGoemetricObject(String color, boolean filled){
        dateCreated = new java.util.Date();
        this.color = color;
        this.filled = filled;
    }

    /** Return color*/
    public String getColor() {
        return color;
    }

    /** Set color*/
    public void setColor(String color) {
        this.color = color;
    }

    /** Return filled. Since filled is a boolean, its getter method is named isFilled*/
    public boolean isFilled() {
        return filled;
    }

    /** Set a new filled*/
    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    /** dateCreated*/
    public Date getDateCreated() {
        return dateCreated;
    }

    /** Return a string representation of this object*/
    @Override
    public String toString() {
        return "Task_11_0_1_SimpleGoemetricObject{" +
                "color='" + color + '\'' +
                ", filled=" + filled +
                ", dateCreated=" + dateCreated +
                '}';
    }
}
