package introductionToJavaProgramming.unit_11_InheritenceAndPolymorphism;

public class Task_11_0_2_CircleFromSimpleGeometricObject extends Task_11_0_1_SimpleGoemetricObject{

    private double radius;

    public Task_11_0_2_CircleFromSimpleGeometricObject(){}

    public Task_11_0_2_CircleFromSimpleGeometricObject(double radius){
        this.radius = radius;
    }

    public Task_11_0_2_CircleFromSimpleGeometricObject(double radius, String color, boolean filled){
        this.radius = radius;
        setColor(color);
        setFilled(filled);
    }

    // Return radius
    public double getRadius() {
        return radius;
    }

    // Set a new radius
    public void setRadius(double radius) {
        this.radius = radius;
    }

    // Return area
    public double getArea(){
        return radius * radius * Math.PI;
    }

    // Return perimeter
    public double getPerimeter(){
        return 2 * radius * Math.PI;
    }

    // Print the circle info
    public void printCircle(){
        System.out.println("The circle is created " + getDateCreated() +" and the radius is " + radius);
    }
}