package introductionToJavaProgramming.unit_13_AbstractClassesAndInterfaces;

import java.util.Date;

public abstract class Task_13_0_1_GeometricObject {

    private String color = "white";
    private boolean filled;
    private java.util.Date dateCreated;

    //Construct a default geometric object
    public Task_13_0_1_GeometricObject() {
        dateCreated = new java.util.Date();
    }

    //Construct a geometric object with color and filled value
    public Task_13_0_1_GeometricObject (String color, boolean filled){
        dateCreated = new java.util.Date();
        this.color = color;
        this.filled = filled;
    }

    //Return color
    public String getColor(){
        return color;
    }

    // Set a new color
    public void setColor(String color) {
        this.color = color;
    }

    // Return filled. Since filled is boolean, the get method is named is Filled
    public boolean isFilled() {
        return filled;
    }

    // Set a new filled
    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    // Get dateCreated
    public Date getDateCreated() {
        return dateCreated;
    }

    @Override
    public String toString() {
        return "Task_13_0_1_GeometricObject{" +
                "color='" + color + '\'' +
                ", filled=" + filled +
                ", dateCreated=" + dateCreated +
                '}';
    }

    // Abstract method getArea
    public abstract double getArea();

    // Abstract method getPerimeter
    public abstract double getPerimeter();
}
