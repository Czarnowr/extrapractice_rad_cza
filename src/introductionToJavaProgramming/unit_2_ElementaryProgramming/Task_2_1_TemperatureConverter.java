package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Formatting
import java.text.DecimalFormat;
// imported for Scanner
import java.util.Scanner;

public class Task_2_1_TemperatureConverter {
    public static void main(String[] args) {

    /*
    Task 2.1, page 69

    (Convert Celsius to Fahrenheit) Write a program that reads a Celsius degree in a double value from
    the console, then converts it to Fahrenheit and displays the result. The formula for the conversion
    is as follows: fahrenheit = (9 / 5) * celsius + 32
     */

//        // display version
//        System.out.println();
//        System.out.println("Converting manually:");
//        System.out.println();
//
//        // variables for test temperature values
//        int fahrenheit32 = 32; // 32°F
//        int fahrenheit212 = 212; // 212 °F
//        int celsius0 = 0; // 0°C
//        int celsius100 = 100; // 100°C
//
//
//        // maths (magic) happens here
//        int fahrenheit32toC = (fahrenheit32 - 32) * 5 / 9;
//        int fahrenheit212toC = (fahrenheit212 - 32) * 5 / 9;
//        int celsius0toF = (celsius0 * 9 / 5) + 32;
//        int celsius100toF = (celsius100 * 9 / 5) + 32;
//
//
//        // test for Fahrenheit 32
//        System.out.println("Fahrenheit to Celsius:");
//        System.out.println("Fahrenheit: " + fahrenheit32);
//        System.out.println("Celsius: " + fahrenheit32toC);
//        System.out.println("");
//        // test for Fahrenheit 212
//        System.out.println("Fahrenheit to Celsius:");
//        System.out.println("Fahrenheit: " + fahrenheit212);
//        System.out.println("Celsius: " + fahrenheit212toC);
//        System.out.println("");
//        // test for Celsius 0
//        System.out.println("Celsius to Fahrenheit:");
//        System.out.println("Celsius: " + celsius0);
//        System.out.println("Fahrenheit: " + celsius0toF);
//        System.out.println("");
//        // test for Celsius 100
//        System.out.println("Celsius to Fahrenheit:");
//        System.out.println("Celsius: " + celsius100);
//        System.out.println("Fahrenheit: " + celsius100toF);

        // variables
        String choice;
        double numOfCelsius;
        double numOfFahrenheit;
        double celToFar;
        double farToCel;


        // initiate scanner
        Scanner scan = new Scanner(System.in);


        // Scanner with extra levels of security:
        // first: checks for a/A or b/B (choice of function)
        // if input is correct - the next step is executed, if not - the step is repeated until correct input
        // second: in each function checks for correct input (double), if not correct the step is repeated
        // the loop only ends after all inputs were correct

        boolean readyToExit = false;
        while (!readyToExit) {
            // ask for a letter representing the direction of change
            System.out.println("Choose one: \n A: Celsius to Fahrenheit \n B: Fahrenheit to Celsius");
            choice = scan.next();


            // if 'a' or 'A' was entered: execute Celsius to Fahrenheit converter.
            if (choice.toUpperCase().equals("A")) {
                System.out.println("Enter the temperature in Celsius:");
                while (true) {
                    try {
                        numOfCelsius = Double.parseDouble(scan.next());
                        break;
                    } catch (NumberFormatException ignore) {
                        System.out.println("This is not a number, please try again: ");
                    }
                }
                celToFar = ((numOfCelsius * 9 / 5) + 32);

                // changing the input from the user into String and checking the length of the Strings
                String sNumOfCelsius = Double.toString(numOfCelsius);
                int sNumOfCelsiusLength = sNumOfCelsius.length();
                //checking the number of decimal places in the Strings representing input
                int sNumOfCelsiusInt = sNumOfCelsius.indexOf('.');
                int sNumOfCelsiusDec = sNumOfCelsiusLength - sNumOfCelsiusInt - 1;


                DecimalFormat decAccu = new DecimalFormat();
                decAccu.setMaximumFractionDigits(sNumOfCelsiusDec);

                System.out.println();
                System.out.println(decAccu.format(numOfCelsius) + " Celsius equals " + decAccu.format(celToFar) + " Fahrenheit");
                readyToExit = true;


                // if 'b' or 'B' was entered: execute Fahrenheit to Celsius converter.
            } else if (choice.toUpperCase().equals("B")) {
                System.out.println("Enter the temperature in Fahrenheit:");

                while (true) {
                    try {
                        numOfFahrenheit = Double.parseDouble(scan.next());
                        break;
                    } catch (NumberFormatException ignore) {
                        System.out.println("This is not a number, please try again: ");
                    }
                }
                farToCel = ((numOfFahrenheit - 32) * 5 / 9);

                // changing the input from the user into String and checking the length of the Strings
                String sNumOfFahrenheit = Double.toString(numOfFahrenheit);
                int sNumOfFahrenheitLength = sNumOfFahrenheit.length();
                //checking the number of decimal places in the Strings representing input
                int sNumOfFahrenheitInt = sNumOfFahrenheit.indexOf('.');
                int sNumOfFahrenheitDec = sNumOfFahrenheitLength - sNumOfFahrenheitInt - 1;


                DecimalFormat decAccu = new DecimalFormat();
                decAccu.setMaximumFractionDigits(sNumOfFahrenheitDec);


                System.out.println();
                System.out.println(decAccu.format(numOfFahrenheit) + " Fahrenheit equals " + decAccu.format(farToCel) + " Celsius");
                readyToExit = true;
            } else {
                System.out.println("Try again.");
            }
        }
    }
}
