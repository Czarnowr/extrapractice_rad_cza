package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_5_CalculateTips {
    public static void main(String[] args) {

        /*
        Task 2.5, page 70

        (Financial application: calculate tips) Write a program that reads the subtotal and the gratuity
        rate, then computes the gratuity and total. For example, if the user enters 10 for subtotal and
        15% for gratuity rate, the program displays $1.5 as gratuity and $11.5 as total.
         */

        // variables
        double subtotal;
        double gratuityRate;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter subtotal: ");
            try {
                subtotal= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException e) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();


        // ask for number 2, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the gratuity rate: ");
            try {
                gratuityRate= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException e) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();


        // Mathematics (magic) happens here
        double tip = subtotal * gratuityRate / 100;
        double total = subtotal + tip;


        // Print result to console
        System.out.println("The gratuity is " + tip + "% and the total is $" + total);

    }
}
