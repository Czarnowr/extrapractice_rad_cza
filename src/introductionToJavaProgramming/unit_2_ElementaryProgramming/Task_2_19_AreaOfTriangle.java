package introductionToJavaProgramming.unit_2_ElementaryProgramming;

public class Task_2_19_AreaOfTriangle {
    public static void main(String[] args) {

        /*
        Task 2.19, page 73

        (Geometry: area of a triangle) Write a program that prompts the user to enter three points (x1, y1),
        (x2, y2), (x3, y3) of a triangle and displays its area.

        The formula for computing the area of a triangle is

        s = (side1 + side2 + side3)/2;
        area = Math.pow((s(s - side1)(s - side2)(s - side3)), 0.5)
         */
    }
}
