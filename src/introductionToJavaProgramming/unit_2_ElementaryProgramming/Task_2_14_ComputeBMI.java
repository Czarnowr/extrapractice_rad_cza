package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_14_ComputeBMI {
    public static void main(String[] args) {
        /*
        Task 2.14, page 72

        (Health application: computing BMI) Body Mass Index (BMI) is a measure of health on weight. It can be
        calculated by taking your weight in kilograms and dividing by the square of your height in meters. Write
        a program that prompts the user to enter a weight in pounds and height in inches and displays the BMI.

        Note that one pound is 0.45359237 kilograms and one inch is 0.0254 meters.
         */


        // variables
        double height;
        double weight;
        double calculateBMI;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter your height: ");
            try {
                height= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter your weight: ");
            try {
                weight= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();


        // mathematics (magic) happens here
        calculateBMI = weight / Math.pow(height, 2);


        // printing results to console
        System.out.println("Your BMI is: " + calculateBMI);
    }
}
