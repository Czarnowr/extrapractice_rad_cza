package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_16_AreaOfHexagen {
    public static void main(String[] args) {

    /*
    Task 2.16, page 72

    (Geometry: area of a hexagon) Write a program that prompts the user to enter the side of a hexagon and displays
    its area. The formula for computing the area of a hexagon is:

    Area = ((3* root(3))/2)*Math.pow(s,2)

    where s is the length of a side.
     */


        // variables
        double hexagonSide;
        double hexagonArea;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the size of the side of the hexagon: ");
            try {
                hexagonSide= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }


        // mathematics (magic) happens here
//        Area = ((3* root(3))/2)*Math.pow(s,2)
        hexagonArea = ((3 * Math.pow(3, 0.5) / 2) * Math.pow(hexagonSide, 2));
        System.out.println("The hexagon area is: " + hexagonArea);
    }
}
