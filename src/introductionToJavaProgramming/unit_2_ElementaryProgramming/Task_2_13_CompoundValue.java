package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_13_CompoundValue {
    public static void main(String[] args) {

        /*
        Task 2.13, page 72

        (Financial application: compound value) Suppose you save $100 each month into a savings account with the
        annual interest rate 5%. Thus, the monthly interest rate is 0.05/12 = 0.00417.

        After the first month, the value in the account becomes 100 * (1 + 0.00417) = 100.417

        After the second month, the value in the account becomes (100 + 100.417) * (1 + 0.00417) = 201.252

        After the third month, the value in the account becomes (100 + 201.252) * (1 + 0.00417) = 302.507 and so on.

        Write a program that prompts the user to enter a monthly saving amount and displays the account value after
        the sixth month.
         */

        // variables
        double monthlySavingsAmount;
        double annualInterestRate;
        double accountBalance = 0;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the monthly savings amount: ");
            try {
                monthlySavingsAmount= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the annual interest rate in %: ");
            try {
                annualInterestRate= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();

        double monthlyInterestRate = annualInterestRate / 12 / 100;

        // mathematics (magic) happens here:
        for (int i=1; i <=6; i++){
            accountBalance = (accountBalance + monthlySavingsAmount) * (1 + monthlyInterestRate);
            System.out.println("Account balance after " + i + " months is " + accountBalance);
        }
    }
}
