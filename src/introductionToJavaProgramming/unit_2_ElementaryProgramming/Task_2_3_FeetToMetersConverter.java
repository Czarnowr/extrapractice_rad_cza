package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_3_FeetToMetersConverter {
    public static void main(String[] args) {

        /*
        Task 2.3, page 69

        (Convert feet into meters) Write a program that reads a number in feet, converts it to meters,
        and displays the result. One foot is 0.305 meter.
         */

        // variables
        String choice;
        final double FOOT_IN_METERS = 0.305;
        final double METER_IN_FEET = 3.281;
        double numberOfFeet;
        double numberOfMeters;

        // import Scanner
        Scanner scan = new Scanner(System.in);


        // Scanner with extra levels of security:
        // first: checks for a/A or b/B (choice of function)
        // if input is correct - the next step is executed, if not - the step is repeated until correct input
        // second: in each function checks for correct input (double), if not correct the step is repeated
        // the loop only ends after all inputs were correct

        boolean readyToExit = false;
        while (!readyToExit) {
            // ask for a letter representing the direction of change
            System.out.println("Choose one: \n A: Feet to Meters \n B: Meters to Feet");
            choice = scan.next();


            // if 'a' or 'A' was entered: execute Feet to Meters converter.
            if (choice.toUpperCase().equals("A")) {
                System.out.println("Enter the length in feet:");
                while (true) {
                    try {
                        numberOfFeet = Double.parseDouble(scan.next());
                        break;
                    } catch (NumberFormatException ignore) {
                        System.out.println("This is not a number, please try again: ");
                    }
                }

                double feetToMeters = numberOfFeet * FOOT_IN_METERS;

                System.out.println(numberOfFeet + " feet equals " + feetToMeters + " meters.");

                        readyToExit = true;


                // if 'b' or 'B' was entered: execute Meters to Feet converter.
            } else if (choice.toUpperCase().equals("B")) {
                System.out.println("Enter the length in meters:");

                while (true) {
                    try {
                        numberOfMeters = Double.parseDouble(scan.next());
                        break;
                    } catch (NumberFormatException ignore) {
                        System.out.println("This is not a number, please try again: ");
                    }
                }

                double metersToFeet = numberOfMeters * METER_IN_FEET;

                System.out.println(numberOfMeters + " meters equals " + metersToFeet + " feet.");

                        readyToExit = true;

            } else {
                System.out.println("Try again.");
            }

        }
    }
}