package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_9_AverageAcceleration {
    public static void main(String[] args) {

        /*
        Task 2.9, page 71

        (Physics: acceleration) Average acceleration is defined as the change of velocity divided by the time taken
        to make the change, as shown in the following formula:

        a = (v1 - v0) / t

        Write a program that prompts the user to enter the starting velocity v0 in meters/second, the ending velocity
        v1 in meters/second, and the time span t in seconds, and displays the average acceleration.
         */

        //variables
        double startingVelocity;
        double endingVelocity;
        double timeSpan;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for a double, check if input is a double, if not, try again
        while (true) {
            System.out.println("Please enter the Starting Velocity (meters / second): ");
            try {
                startingVelocity= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again!");
                System.out.println();
            }
        }
        System.out.println();

        // ask for a double, check if input is a double, if not, try again
        while (true) {
            System.out.println("Please enter the Ending Velocity (meters / second): ");
            try {
                endingVelocity= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again!");
                System.out.println();
            }
        }
        System.out.println();

        // ask for a double, check if input is a double, if not, try again
        while (true) {
            System.out.println("Please enter the complete time (seconds): ");
            try {
                timeSpan= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // mathematics (magic) happens here
        double averageVelocity = (endingVelocity - startingVelocity) / timeSpan;


        // print result to console
        System.out.println("The average velocity is " + averageVelocity + " meters/second");
    }
}
