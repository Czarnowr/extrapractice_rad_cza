package introductionToJavaProgramming.unit_2_ElementaryProgramming;

public class Task_2_0_7_ShowCurrentTime {
    public static void main(String[] args) {

        /*
        Listing 2.7, page 52

        Develop a program that displays the current time in GMT (Greenwich Mean Time) in the format
        hour:minute:second, such as 13:19:8. The currentTimeMillis method in the System class.
         */

        //variables
        long currentTimeMilliSeconds = System.currentTimeMillis();


        // mathematics (magic) happens here
        long currentTimeSeconds = currentTimeMilliSeconds / 1000;
        long currentSecond = currentTimeSeconds % 60;

        long currentTimeMinutes = currentTimeSeconds / 60;
        long currentMinutes = currentTimeMinutes % 60;

        long currentTimeHours = currentTimeMinutes / 60;
        //long currentHours12 = currentTimeHours % 60;
        long currentHours24 = currentTimeHours % 24;


        // print results to console
        System.out.println("Current time GMT is: " + currentHours24 + ":" + currentMinutes + ":" + currentSecond);
    }
}
