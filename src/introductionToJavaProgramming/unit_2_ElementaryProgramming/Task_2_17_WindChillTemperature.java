package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner

import java.util.Scanner;

public class Task_2_17_WindChillTemperature {
    public static void main(String[] args) {

        /*
        Task 2.17, page 73

        (Science: wind-chill temperature) How cold is it outside? The temperature alone is not enough to provide
        the answer. Other factors including wind speed, relative humidity, and sunshine play important roles in
        determining coldness outside.

        In 2001, the National Weather Service (NWS) implemented the new wind-chill temperature to measure the
        coldness using temperature and wind speed. The formula is:

            windChillTemperature = 26.74 + (0.6215*outsideTemperature)- (35.75 * Math.pow(windSpeed, 0.16) + (0.4275 * outsideTemperature * Math.pow(windSpeed, 0.16)

        where ta is the outside temperature measured in degrees Fahrenheit and v is the speed measured in miles
        per hour. twc is the wind-chill temperature. The formula cannot be used for wind speeds below 2 mph or
        temperatures below -58 ºF or above 41ºF.

        Write a program that prompts the user to enter a temperature between -58 ºF and 41ºF and a wind speed
        greater than or equal to 2 and displays the wind-chill temperature.
        */


        // variables
        double windChillTemperature;
        double outsideTemperature;
        double windSpeed;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter temperature between -58ºF and 41ºF: ");
            try {
                outsideTemperature = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter wind speed higher than to or equal 2mph: ");
            try {
                windSpeed = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // mathematics (magic) happens here + printing to command line
        if (outsideTemperature >= -58 && outsideTemperature <= 41 && windSpeed >= 2) {
            windChillTemperature = 35.74 + (0.6215 * outsideTemperature) - (35.75 * Math.pow(windSpeed, 0.16)) + (0.4275 * outsideTemperature * Math.pow(windSpeed, 0.16));
            System.out.println("Wind Chill Temperature equals: " + windChillTemperature);
        } else if (outsideTemperature < -58 || outsideTemperature > 41 || windSpeed < 2) {
            System.out.println("The temperature and / or wind values are outside of the allowed range!");
        }
    }
}
