package introductionToJavaProgramming.unit_2_ElementaryProgramming;

import java.util.Scanner;

public class Task_2_7_NumberOfYearsFromMinutes {
    public static void main(String[] args) {

        /*
        Task 2.7, page 70

        (Find the number of years) Write a program that prompts the user to enter the minutes (e.g., 1 billion), and
        displays the number of years and days for the minutes. For simplicity, assume a year has 365 days.
         */


        // variables
        int numberOfMinutes;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for an integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter the number of minutes: ");
            try {
                numberOfMinutes = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // Mathematics (magic) happens here
        int numOfYears;
        int numOfDays;

        numOfYears = numberOfMinutes / 525600;
        numOfDays = numberOfMinutes % 525600;
        numOfDays /= 1440;


        // print result to console
        if (numOfYears == 0) {
            if (numOfDays < 1){
                System.out.println(numberOfMinutes + " of minutes is less than one day");
            }else {
                System.out.println(numberOfMinutes + " of minutes is approximately " + numOfDays + " days");
            }
        }else {
            System.out.println(numberOfMinutes + " of minutes is approximately " + numOfYears + " years and " + numOfDays + " days");
        }
    }
}
