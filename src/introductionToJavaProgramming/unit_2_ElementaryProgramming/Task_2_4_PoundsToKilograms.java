package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_4_PoundsToKilograms {
    public static void main(String[] args) {

        /*
        Task 2.4, page 70

        (Convert feet into meters) Write a program that reads a number in feet, converts it to meters,
        and displays the result. One foot is 0.305 meter.
         */

        // variables
        String choice;
        final double POUND_IN_KILOGRAMS = 0.454;
        final double KILOGRAMS_IN_POUNDS = 2.205;
        double weightInPounds;
        double weightInKilograms;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // Scanner with extra levels of security:
        // first: checks for a/A or b/B (choice of function)
        // if input is correct - the next step is executed, if not - the step is repeated until correct input
        // second: in each function checks for correct input (double), if not correct the step is repeated
        // the loop only ends after all inputs were correct


        boolean readyToExit = false;
        while (!readyToExit) {
            // ask for a letter representing the direction of change
            System.out.println("Choose one: \n A: Convert Pounds to Kilograms \n B: Convert Kilograms to Pounds");
            choice = scan.next();


            // if 'a' or 'A' was entered: execute Pounds to Kilograms converter.
            if (choice.toUpperCase().equals("A")) {
                System.out.println("Enter the weight in pounds:");
                while (true) {
                    try {
                        weightInPounds = Double.parseDouble(scan.next());
                        break;
                    } catch (NumberFormatException ignore) {
                        System.out.println("This is not a number, please try again: ");
                    }
                }


                // Mathematics (magic) happens here
                double poundsToKilograms = weightInPounds * POUND_IN_KILOGRAMS;


                // Printing result to console
                System.out.println(weightInPounds + " pounds is " + poundsToKilograms + " kilograms");


                        readyToExit = true;


                // if 'b' or 'B' was entered: execute Kilograms to Pounds converter.
            } else if (choice.toUpperCase().equals("B")) {
                System.out.println("Enter the weight in kilograms:");

                while (true) {
                    try {
                        weightInKilograms = Double.parseDouble(scan.next());
                        break;
                    } catch (NumberFormatException ignore) {
                        System.out.println("This is not a number, please try again: ");
                    }
                }


                // Mathematics (magic) happens here
                double kilogramsToPounds = weightInKilograms * KILOGRAMS_IN_POUNDS;


                // Printing result to console
                System.out.println(weightInKilograms + " pounds is " + kilogramsToPounds + " kilograms");


                readyToExit = true;

            } else {

                System.out.println("Try again.");

            }
        }

    }
}
