package introductionToJavaProgramming.unit_2_ElementaryProgramming;

import java.util.Scanner;

public class Task_2_2_CylinderVolumeCalculator {
    public static void main(String[] args) {

        /*
        Task 2.2, page 69

        (Compute the volume of a cylinder) Write a program that reads in the radius and length of a cylinder
        and computes the area and volume using the following formulas:

        area = radius * radius * PI
        volume = area * length
         */

        // variables
        double cylinderRadius;
        double cylinderLength;
        final double PI = 3.14;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter number 1: ");
            try {
                cylinderRadius = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();

        // ask for number 2, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter number 1: ");
            try {
                cylinderLength = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();


        // Mathematics (magic) happens here
        double cylinderArea = cylinderRadius * cylinderRadius *PI;
        double cylinderVolume = cylinderArea * cylinderLength;


        // Printing results into console
        System.out.println("The area of the cylinder (length: " + cylinderLength + ",radius: " + cylinderRadius + " is: " + cylinderArea + "\n");
        System.out.println("The volume of the cylinder (length: " + cylinderLength + ",radius: " + cylinderRadius + " is: " + cylinderVolume + "\n");

    }
}
