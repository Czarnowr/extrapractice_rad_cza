package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_20_CalculateInterest {
    public static void main(String[] args) {

        /*
        Task 2.20, page 74

        (Financial application: calculate interest) If you know the balance and the annual percentage interest rate,
        you can compute the interest on the next monthly payment using the following formula:

            interest = balance * (annualInterestRate/1200)

        Write a program that reads the balance and the annual percentage interest rate and displays the interest
        for the next month.
        */


        // variables
        double balance;
        double interest;
        double annualInterestRate;

        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the account balance: ");
            try {
                balance= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the annual interest rate: ");
            try {
                annualInterestRate= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }


        // mathematics (magic) happens here
        interest = balance * (annualInterestRate/1200);


        // print results to console
        System.out.println("The next monthly interest is: " + interest);
    }
}
