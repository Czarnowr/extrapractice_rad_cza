package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner

import java.util.Scanner;

public class Task_2_11_PopulationProjectionWithScanner {
    public static void main(String[] args) {
         /*
         Task 2.11, page 71

        (Population projection) Rewrite Programming Exercise 1.11 to prompt the user to enter the number of years
        and displays the population after the number of years.

        Use the hint in Programming Exercise 1.11 for this program. The population should be cast into an integer.
         */

        // variables
        final double NUMBER_OF_DAYS = 365;
        final double NUMBER_OF_HOURS = NUMBER_OF_DAYS * 24;
        final double NUMBER_OF_MINUTES = NUMBER_OF_HOURS * 60;
        final double NUMBER_OF_SECONDS = NUMBER_OF_MINUTES * 60;
        double currentPopulation = 312032486;
        double birthsPerYear = NUMBER_OF_SECONDS / 7;
        double deathsPerYear = NUMBER_OF_SECONDS / 13;
        double immigrationIncrease = NUMBER_OF_SECONDS / 45;
        int numberOfYears;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for an integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter the number of years for the Population Projection: ");
            try {
                numberOfYears = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // mathematics (magic) happens here
        if (numberOfYears < 0) {
            System.out.println("Projections do not go backwards!");
        } else if (numberOfYears == 0) {
            System.out.println("Current population is: " + (int) currentPopulation);
        } else if (numberOfYears == 1) {
            currentPopulation = currentPopulation + numberOfYears * (birthsPerYear - deathsPerYear + immigrationIncrease);
            System.out.println("Population after 1 year is: " + (int) currentPopulation);
        } else {
            currentPopulation = currentPopulation + numberOfYears * (birthsPerYear - deathsPerYear + immigrationIncrease);
            System.out.println("Population after" + numberOfYears + " years is: " + (int) currentPopulation);
        }
    }
}
