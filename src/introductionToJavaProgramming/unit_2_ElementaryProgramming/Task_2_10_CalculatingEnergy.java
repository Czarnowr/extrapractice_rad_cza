package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_10_CalculatingEnergy {
    public static void main(String[] args) {

        /*
        Task 2.10, page 71

        (Science: calculating energy) Write a program that calculates the energy needed to heat water from an
        initial temperature to a final temperature. Your program should prompt the user to enter the amount of
        water in kilograms and the initial and final temperatures of the water.

        The formula to compute the energy is Q = M * (finalTemperature – initialTemperature) * 4184, where M is
        the weight of water in kilograms, temperatures are in degrees Celsius, and energy Q is measured in joules.
         */

        // variables
        double initialTemperature;
        double finalTemperature;
        double amountOfWater;
        double energyNeeded;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the initial temperature in Celsius: ");
            try {
                initialTemperature = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the final temperature in Celsius: ");
            try {
                finalTemperature = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the weight of water in kilograms: ");
            try {
                amountOfWater = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();


        // mathematics (magic) happens here
        energyNeeded = amountOfWater * (finalTemperature - initialTemperature) * 4184;


        // print results to console
        System.out.println("The amount of energy needed to heat " + amountOfWater + " kg of water from "
                + initialTemperature + " Celsius to " + finalTemperature + "Celsius is " + energyNeeded);
    }
}
