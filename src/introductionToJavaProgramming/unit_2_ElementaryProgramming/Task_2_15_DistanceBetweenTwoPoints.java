package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner

import java.util.Scanner;

public class Task_2_15_DistanceBetweenTwoPoints {
    public static void main(String[] args) {

        /*
        Task 2.15, page 72

       (Geometry: distance of two points) Write a program that prompts the user to enter two points (x1, y1) and
       (x2, y2) and displays their distance between them.

        The formula for computing the distance is root((x2 - x1)² + (y2 - y1)²).

        Note that you can use Math.pow(a, 0.5) to compute root(a).
         */


        // variables
        double pointX1;
        double pointX2;
        double pointY1;
        double pointY2;
        double distanceBetweenPoints;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for an integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter point X1: ");
            try {
                pointX1 = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }

        // ask for an integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter point X2: ");
            try {
                pointX2 = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }

        // ask for an integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter point Y1: ");
            try {
                pointY1 = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }

        // ask for an integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter point Y2: ");
            try {
                pointY2 = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }


        // mathematics (magic) happens here
        distanceBetweenPoints = Math.pow((Math.pow((pointX2 - pointX1), 2) + Math.pow((pointY2 - pointY1), 2)), 0.5);
        System.out.println("The distance between points is: " + distanceBetweenPoints);
    }
}
