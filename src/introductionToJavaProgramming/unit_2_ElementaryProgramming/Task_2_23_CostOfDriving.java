package introductionToJavaProgramming.unit_2_ElementaryProgramming;

import java.util.Scanner;

public class Task_2_23_CostOfDriving {
    public static void main(String[] args) {

        /*
        Task 2.23, page 74

        (Cost of driving) Write a program that prompts the user to enter the distance to drive, the fuel efficiency of
        the car in miles per gallon, and the price per gallon, and displays the cost of the trip. Here is a sample run:
         */

        // variables
        double drivingDistance;
        double milesPerGallon;
        double pricePerGallon;
        double costOfTrip;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again

        while (true) {
            System.out.println("Please enter the driving distance: ");
            try {
                drivingDistance = Double.parseDouble(scan.next());
                break;

            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for number 1, check if input is a number, if not a number try again

        while (true) {
            System.out.println("Please enter the miles per gallon: ");
            try {
                milesPerGallon = Double.parseDouble(scan.next());
                break;

            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for number 1, check if input is a number, if not a number try again

        while (true) {
            System.out.println("Please enter the price per gallon: ");
            try {
                pricePerGallon = Double.parseDouble(scan.next());
                break;

            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }


        // mathematics (magic) happens here
        costOfTrip = drivingDistance * (pricePerGallon / milesPerGallon);


        // printing results to console
        System.out.println("The cost of the trip is: " + costOfTrip);

    }
}
