package introductionToJavaProgramming.unit_2_ElementaryProgramming;

import java.util.Scanner;

public class Task_2_0_10_ComputeChange {
    public static void main(String[] args) {

        /*
         Suppose you want to develop a program that changes a given amount of money into smaller monetary units. The
         program lets the user enter an amount as a double value representing a total in dollars and cents, and outputs
         a report listing the monetary equivalent in the maximum number of dollars, quarters, dimes, nickels, and
         pennies, in this order, to result in the minimum number of coins.
         */


        // variables
        double amountInDollarsAndCents;
        int amountInCents;
        int amountInDollars;
        int amountInQuarters;
        int amountInDimes;
        int amountInNickles;
        int amountInPennies;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again

        while (true) {
            System.out.println("Please enter the amount in dollars and cents: ");
            try {
                amountInDollarsAndCents = Double.parseDouble(scan.next());
                break;

            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }


        // mathematics (magic) happens here
        amountInCents = (int) (amountInDollarsAndCents * 100);

        amountInDollars = amountInCents / 100;
        amountInCents %= 100;

        amountInQuarters = amountInCents / 25;
        amountInCents %= 25;

        amountInDimes = amountInCents / 10;
        amountInCents %= 10;

        amountInNickles = amountInCents / 5;
        amountInCents %= 5;

        amountInPennies = amountInCents;


        // display results to console
        System.out.println("Your amount consists of: ");
        if (amountInDollars != 0) {
            System.out.println(amountInDollars + " dollar coins");
        }
        if (amountInQuarters != 0) {
            System.out.println(amountInQuarters + " quarter coins");
        }
        if (amountInDimes != 0) {
            System.out.println(amountInQuarters + " dime coins");
        }
        if (amountInNickles != 0) {
            System.out.println(amountInNickles + " nickle coins");
        }
        if (amountInPennies != 0) {
            System.out.println(amountInPennies + " penny coins");
        }
    }
}
