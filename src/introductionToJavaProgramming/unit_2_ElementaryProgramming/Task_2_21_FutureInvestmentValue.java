package introductionToJavaProgramming.unit_2_ElementaryProgramming;

import java.util.Scanner;

public class Task_2_21_FutureInvestmentValue {
    public static void main(String[] args) {
        /*
        Task 2.21, page 74

        (Financial application: calculate future investment value) Write a program that reads in investment amount,
        annual interest rate, and number of years, and displays the future investment value using the following formula:

            futureInvestmentValue = investmentAmount * Math.pow((1 + monthlyInterestRate), (numberOfYears*12))

        For example, if you enter amount 1000, annual interest rate 3.25%, and number of years 1, the future investment
        value is 1032.98.
        */

        // variables
        double investmentAmount;
        double annualInterestRate;
        int numberOfYears;
        double monthlyInterestRate;
        double futureInvestmentValue;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again

        while (true) {
            System.out.println("Please enter the investment amount: ");
            try {
                investmentAmount = Double.parseDouble(scan.next());
                break;

            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        while (true) {
            System.out.println("Please enter the annual interest rate in percentage: ");
            try {
                annualInterestRate = Double.parseDouble(scan.next());
                break;

            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        while (true) {
            System.out.println("Please enter the number of years: ");
            try {
                numberOfYears = Integer.parseInt(scan.next());
                break;

            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // mathematics (magic) happens here
        monthlyInterestRate = annualInterestRate / 12 / 100;
        futureInvestmentValue = investmentAmount * Math.pow((1 + monthlyInterestRate), (numberOfYears*12));


        // printing results to console
        System.out.println("The future investment value after " + numberOfYears + " years is: " + futureInvestmentValue);

    }
}
