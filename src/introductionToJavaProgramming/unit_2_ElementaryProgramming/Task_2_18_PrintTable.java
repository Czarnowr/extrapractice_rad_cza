package introductionToJavaProgramming.unit_2_ElementaryProgramming;

public class Task_2_18_PrintTable {
    public static void main(String[] args) {

    /*
    Task 2.18, page 73

    (Print a table) Write a program that displays the following table. Cast floating point numbers into integers.
        a   b   pow(a, b)
        1   2   1
        2   3   8
        3   4   81
        4   5   1024
        5   6   15625
     */

        double i = 0;

        System.out.println("a" + "    " + "b" + "      " + "pow(a,b)");
        for (i = 1; i < 6; i++) {
            double b = i + 1;
            double c = Math.pow(i, b);
            System.out.println((int) i + "    " + (int) b + "      " + (int) c);
        }

    }
}
