package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner
import java.util.Scanner;

public class Task_2_6_SumDigitsInInteger {
    public static void main(String[] args) {

        /*
        Task 2.6, page 70

        (Sum the digits in an integer) Write a program that reads an integer between 0 and 1000 and adds all the digits in the integer.
        For example, if an integer is 932, the sum of all its digits is 14.

        Hint: Use the % operator to extract digits, and use the / operator to remove the extracted digit.
        For instance, 932 % 10 = 2 and 932 / 10 = 93.
         */

        // variables
        int userInput;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // Scanner with extra levels of security:
        // first: checks if input is an integer, if not - repeat
        // second: checks if condition is met, if not - repeat
        // the loop only ends after all inputs were correct

        boolean readyToExit = false;
        while (!readyToExit) {

            // ask for a grade, check if integer
            while (true) {
                System.out.println("Enter an integer between 0 and 1000");
                try {
                    userInput = Integer.parseInt(scan.next());
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("This is not an integer, please try again: ");
                }
            }

            // if between 0 and 1000 - continue
            if (userInput >= 0 && userInput <= 1000) {

                // Mathematics (magic) happens here
                int userInputCopy = userInput;
                int extractedDigit1 = userInputCopy % 10;
                userInputCopy /= 10;
                int extractedDigit2 = userInputCopy % 10;
                userInputCopy /=10;
                int extractedDigit3 = userInputCopy % 10;
                userInputCopy /=10;
                int extractedDigit4 = userInputCopy % 10;
                int digitCount = extractedDigit1 + extractedDigit2 + extractedDigit3 + extractedDigit4;


                // print result to console
                System.out.println("The sum of all digits in " + userInput + " is " + digitCount);


                // if condition is met, finish the loop
                readyToExit = true;

                // if not between 0 and 1000 - loop back
            } else {

                System.out.println("Try again.");
            }
        }
    }
}
