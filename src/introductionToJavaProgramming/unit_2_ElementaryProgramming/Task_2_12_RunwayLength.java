package introductionToJavaProgramming.unit_2_ElementaryProgramming;

// imported for Scanner

import java.util.Scanner;

public class Task_2_12_RunwayLength {
    public static void main(String[] args) {

        /*
        Task 2.12, page 71

        (Physics: finding runway length) Given an airplane’s acceleration a and take-off speed v, you can compute
        the minimum runway length needed for an airplane to take off using the following formula:

        length = v² / 2a

        Write a program that prompts the user to enter v in meters/second (m/s) and the acceleration a in
        meters/second squared (m/s²), and displays the minimum runway length.
         */


        // variables
        double a;
        double v;
        double runwayLength;


        // Initiate Scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the acceleration of the plane: ");
            try {
                a = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the take-off speed of the plane: ");
            try {
                v = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();


        // mathematics (magic) happens here
        runwayLength = Math.pow(v, 2) / (2 * a);


        // print results to console
        System.out.println("The necessary runway length for the given plane is: " + (int)runwayLength + " meters.");
    }
}
