package introductionToJavaProgramming;

import java.util.Arrays;

public class Temp {
    public static void main(String[] args) {
        //tekstowa wersja tablic
        Double[] doubles = {5.5, 1.0, .7, 99., -7.1};
        System.out.println("doubles.toString() = " + doubles.toString());
        System.out.println("Arrays.toString(doubles) = " + Arrays.toString(doubles));
    }
}
