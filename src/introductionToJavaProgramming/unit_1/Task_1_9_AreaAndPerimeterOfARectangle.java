package introductionToJavaProgramming.unit_1;

/**
 * Exercise 1.9, page 31
 */

public class Task_1_9_AreaAndPerimeterOfARectangle {
    public static void main(String[] args) {

        double width = 4.5;
        double height = 7.9;

        System.out.println("Area of a rectangle with width " + width + " and height " + height + " is " + width * height);
    }
}
