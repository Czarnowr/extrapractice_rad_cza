package introductionToJavaProgramming.unit_1;

public class Task_1_11_PopulationProjection {
    public static void main(String[] args){

        /*
        Task 1.11, page 31

        (Population projection) The U.S. Census Bureau projects population based on the following assumptions:
            ■ One birth every 7 seconds
            ■ One death every 13 seconds
            ■ One new immigrant every 45 seconds

        Write a program to display the population for each of the next five years. Assume the current population
        is 312,032,486 and one year has 365 days. Cast the population number into an integer.
         */

        // variables
        final double NUMBER_OF_DAYS = 365;
        final double NUMBER_OF_HOURS = NUMBER_OF_DAYS * 24;
        final double NUMBER_OF_MINUTES = NUMBER_OF_HOURS * 60;
        final double NUMBER_OF_SECONDS = NUMBER_OF_MINUTES * 60;
        double currentPopulation = 312032486;
        double birthsPerYear = NUMBER_OF_SECONDS / 7;
        double deathsPerYear = NUMBER_OF_SECONDS / 13;
        double immigrationIncrease = NUMBER_OF_SECONDS / 45;


        // mathematics (magic) happens here
        for(int i=1; i<=5; i++){
            currentPopulation = currentPopulation + birthsPerYear - deathsPerYear + immigrationIncrease;
            System.out.println("Population after " + i + " year is: " + (int)currentPopulation);
        }

    }
}
