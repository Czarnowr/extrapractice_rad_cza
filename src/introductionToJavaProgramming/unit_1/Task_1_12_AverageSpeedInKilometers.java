package introductionToJavaProgramming.unit_1;

    /*
    Task 1.12, page 31

    Assume a runner runs 24 miles in 1 hour, 40 minutes, and 35 seconds. Write a program that displays the average
    speed in kilometers per hour. (Note that 1 mile is 1.6 kilometers.)
     */

public class Task_1_12_AverageSpeedInKilometers {
    public static void main(String[] args) {

        int timeInHours = 1;
        int timeInMinutes = 40;
        int timeInSeconds = 35;
        int totalTimeInSeconds = (timeInHours*3600) + (timeInMinutes*60) + timeInSeconds;

        double distanceInMiles = 24;
        double distanceInKilometres = distanceInMiles * 1.6;

        double averageSpeed = (distanceInKilometres/totalTimeInSeconds)*3600;

        System.out.println(averageSpeed);
    }
}
