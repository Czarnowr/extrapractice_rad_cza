package introductionToJavaProgramming.unit_1;

/**
 * Exercise 1.5, page 30
 */

public class Task_1_5_ComputeExpressions {
    public static void main(String[] args) {
        System.out.println(((9.5*4.5)-(2.5*3))/(45.5-3.5));
    }
}
