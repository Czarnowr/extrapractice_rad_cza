package introductionToJavaProgramming.unit_1;

/**
 * Exercise 1.7, page 31
 */

public class Task_1_7_ApproximatePi {
    public static void main(String[] args) {
        System.out.println(4*(1.0-(1.0/3)+(1.0/5)-(1.0/7)+(1.0/9)-(1.0/11)));
        System.out.println(4*(1.0-(1.0/3)+(1.0/5)-(1.0/7)+(1.0/9)-(1.0/11)+(1.0/13)));
    }
}
