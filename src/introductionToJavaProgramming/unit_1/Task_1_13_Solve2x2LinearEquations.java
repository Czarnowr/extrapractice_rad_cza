package introductionToJavaProgramming.unit_1;

    /*
    Task 1.13, page 31

    You can use Cramer’s rule to solve the following 2 * 2 system of linear equation:

    ax + by = e     x = (ed - bf) / (ad - bc)
    cx + dy = f     y = (af - ec) / (ad - bc)

    Write a program that solves the following equation and displays the value for x and y:

    3.4x + 50.2y = 44.5
    2.1x + .55y = 5.9
     */

public class Task_1_13_Solve2x2LinearEquations {
    public static void main(String[] args) {

        double valueA = 3.4;
        double valueB = 50.2;
        double valueC = 2.1;
        double valueD = 0.55;
        double valueE = 44.5;
        double valueF = 5.9;

        double valueX = ((valueE * valueD - valueB * valueF)) / ((valueA * valueD) - (valueB * valueC));
        double valueY = ((valueA * valueF - valueE * valueC)) / ((valueA * valueD) - (valueB * valueC));

        System.out.println("X equals: " + valueX);
        System.out.println("Y equals: " + valueY);
    }
}
