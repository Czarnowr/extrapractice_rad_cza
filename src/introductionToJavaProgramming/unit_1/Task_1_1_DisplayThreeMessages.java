package introductionToJavaProgramming.unit_1;

/**
 * Exercise 1.1, page 30
 */

public class Task_1_1_DisplayThreeMessages {
    public static void main(String[] args) {
        System.out.println("Welcome to Java \nWelcome To Computer Science\nProgramming Is Fun");
    }
}
