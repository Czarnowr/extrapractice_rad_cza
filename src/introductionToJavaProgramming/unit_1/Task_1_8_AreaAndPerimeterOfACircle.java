package introductionToJavaProgramming.unit_1;

/**
 * Exercise 1.8, page 31
 */

public class Task_1_8_AreaAndPerimeterOfACircle {
    public static void main(String[] args) {
        double radius = 5.5;
        final double PI = 3.14;

        System.out.println("Perimeter of a circle with radius " + radius + " equals " + 2 * radius * PI);
        System.out.println("Area of a circle with radius " + radius + " equals " + radius * radius * PI);
    }
}
