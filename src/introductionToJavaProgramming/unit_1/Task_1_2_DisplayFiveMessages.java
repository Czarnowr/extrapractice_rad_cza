package introductionToJavaProgramming.unit_1;

/**
 * Exercise 1.2, page 30
 */

public class Task_1_2_DisplayFiveMessages {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            System.out.println("Welcome to Java");
        }
    }
}
