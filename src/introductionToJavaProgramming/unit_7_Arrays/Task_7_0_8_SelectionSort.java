package introductionToJavaProgramming.unit_7_Arrays;

public class Task_7_0_8_SelectionSort {
    public static void main(String[] args) {

        double[] list = {1, 9, 4.5, 6.6, 5.7, -4.5};

        // print the the array above
        for (int i = 0; i < list.length; i++){
            System.out.println(list[i]);
        }
        selectionSort(list);
        System.out.println();

        // print the the array above
        for (int i = 0; i < list.length; i++){
            System.out.println(list[i]);
        }

    }
    // method for sorting
    public static void selectionSort(double[] list) {
        // Find the minimum in the list [i...list.length-1]
        for (int i = 0; i < list.length - 1; i++) {
            double currentMin = list[i];
            int currentMinIndex = i;

            for (int j = i + 1; j < list.length; j++){
                if (currentMin > list[j]){
                    currentMin = list[j];
                    currentMinIndex = j;
                }
            }

            // Swap list [i] with list[currentMinIndex] if necessary
            if (currentMinIndex !=i){
                list [currentMinIndex] = list[i];
                list [i] = currentMin;
            }
        }
    }
}
