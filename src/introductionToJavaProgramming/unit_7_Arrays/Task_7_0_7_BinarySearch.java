package introductionToJavaProgramming.unit_7_Arrays;

public class Task_7_0_7_BinarySearch {
    public static void main(String[] args) {

        int[] list = {2, 4, 7, 10, 11, 45, 50, 59, 60, 66, 69, 70, 79};
//        int i = binarySearch(list, 2); // Returns 0
//        int j = binarySearch(list, 11); // Returns 4
//        int k = binarySearch(list, 12); // Returns –6
//        int l = binarySearch(list, 1); // Returns –1
//        int m = binarySearch(list, 3); // Returns –2

        // execute binarySearch and print the result to console (negative value indicates the key is not in the array,
        // and the number indicates the index where the key would fit if it were there
        System.out.println(binarySearchResultPrint(list, 2));
        System.out.println(binarySearchResultPrint(list, 11));
        System.out.println(binarySearchResultPrint(list, 12));
        System.out.println(binarySearchResultPrint(list, 1));
        System.out.println(binarySearchResultPrint(list, 3));
    }

    // using binary search to find
    public static int binarySearch(int[]list, int key){
        int low = 0;
        int high = list.length-1;

        while(high>=low){
            int mid = (low + high)/2;
            if(key<list[mid]){
                high = mid-1;
            }else if(key==list[mid]){
                return mid;
            }else{
                low = mid+1;
            }
        }
        return -low-1; // Now high < low, key not found
    }

    // printing results of the above method to console
    public static String binarySearchResultPrint(int[] list, int a) {
        int linearSearchOutput = binarySearch(list, a);
        if (linearSearchOutput == -1) {
            return "The given value is not in the array";
        } else {
            return "The index of the array is " + linearSearchOutput;
        }

    }
}
