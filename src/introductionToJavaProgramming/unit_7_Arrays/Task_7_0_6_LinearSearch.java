package introductionToJavaProgramming.unit_7_Arrays;

public class Task_7_0_6_LinearSearch {
    public static void main(String[] args) {

        int[] list = {1, 4, 4, 2, 5, -3, 6, 2};
//        int i = linearSearch(list, 4);
//        int j = linearSearch(list, -4);
//        int k = linearSearch(list, -3);

        // execute linearSearch and print the result to console
        System.out.println(linearSearchResultPrint(list, 4));
        System.out.println(linearSearchResultPrint(list, -4));
        System.out.println(linearSearchResultPrint(list, -3));

    }

    // method for finding a key in the array
    public static int linearSearch(int[] list, int key) {
        for (int i = 0; i < list.length; i++) {
            if (key == list[i]) {
                return i;
            }
        }
        return -1;
    }

    public static String linearSearchResultPrint(int[] list, int a) {
        int linearSearchOutput = linearSearch(list, a);
        if (linearSearchOutput == -1) {
            return "The given value is not in the array";
        } else {
            return "The index of the array is " + linearSearchOutput;
        }

    }

}
