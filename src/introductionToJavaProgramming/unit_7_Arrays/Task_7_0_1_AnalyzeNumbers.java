package introductionToJavaProgramming.unit_7_Arrays;

// imported for Scanner

import java.util.Scanner;

public class Task_7_0_1_AnalyzeNumbers {
    public static void main(String[] args) {

        /*
        Listing 7.1, page 253

        The problem is to read 100 numbers, get the average of these numbers, and find the number of the items greater
        than the average. To be flexible for handling any number of input, we will let the user enter the number of
        input, rather than fixing it to 100. Listing 7.1 gives a solution.
         */


        //initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for a number representing the size of the array
        System.out.println("Please enter the size of the array: ");
        int n = scan.nextInt();


        // variables
        double[] numbers = new double[n];
        double sum = 0;


        // get numbers from the user and add every number to the 'sum' variable
        System.out.println("Please enter the numbers into the array: ");
        for (int i = 0; i < n; i++) {
            numbers[i] = scan.nextDouble();
            sum += numbers[i];
        }


        // calculate average using the 'sum' and the 'n'
        double average = sum / n;


        // check each number and compare it to the average calculated earlier
        int count = 0;
        for (int i = 0; i < n; i++){
            if (numbers[i]>average){
                count++;
            }
        }


        System.out.println("Average is " + average);
        System.out.println("The number of items higher than average is " + count);
    }
}
