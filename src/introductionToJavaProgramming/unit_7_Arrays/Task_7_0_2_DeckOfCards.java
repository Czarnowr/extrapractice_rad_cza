package introductionToJavaProgramming.unit_7_Arrays;

public class Task_7_0_2_DeckOfCards {
    public static void main(String[] args) {

        /*
        Listing 7.2, page 255

        Say you want to write a program that will pick four cards at random from a deck of 52 cards. All the cards can
        be represented using an array named deck, filled with initial values 0 to 51, as follows:

            int[] deck = new int[52];

            // Initialize cards
            for (int i = 0; i < deck.length; i++)
                deck[i] = i;

        Card numbers 0 to 12, 13 to 25, 26 to 38, and 39 to 51 represent 13 Spades, 13 Hearts, 13 Diamonds, and 13
        Clubs, respectively, as shown in Figure 7.2. cardNumber / 13 determines the suit of the card and cardNumber
        % 13 determines the rank of the card, as shown in Figure 7.3. After shuffling the array deck, pick the first
        four cards from deck. The program displays the cards from these four card numbers.
         */

        // create a deck of cards ( a collection of integers from 1 to 52
        int[] deck = new int[52];
        for (int i = 0; i < deck.length; i++) {
            deck[i] = i;
        }


        // create arrays for suit and rank names
        String[] suits = {"Spades", "Hearts", "Diamonds", "Clubs"};
        String[] ranks = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};


        // Shuffle the cards
        for (int i = 0; i < deck.length; i++) {
            int index = (int) (Math.random() * deck.length);
            int temp = deck[i];
            deck[i] = deck[index];
            deck[index] = temp;
        }


        // print the first four cards of the shuffled deck
        for (int i = 0; i < 4; i++) {
            String suit = suits[deck[i] / 13];
            String rank = ranks[deck[i] % 13];
            System.out.println("Card number " + deck[i] + ": " + rank + " of " + suit);
        }


//        // choose four random cards and print
//        for (int i = 0; i < 4; i++) {
//            int cardNumber = (int)(Math.random() * deck.length);
//            String suit = suits[cardNumber / 13];
//            String rank = ranks[cardNumber % 13];
//            System.out.println("Card number " + cardNumber + ": "
//                    + rank + " of " + suit);
//        }
    }
}
