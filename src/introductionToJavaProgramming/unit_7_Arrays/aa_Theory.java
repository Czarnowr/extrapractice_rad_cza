package introductionToJavaProgramming.unit_7_Arrays;

import java.util.Arrays;
import java.util.Scanner;

public class aa_Theory {
    public static void main(String[] args) {

//        /** Version 1 - declaring, creating and assigning values step by step*/
//
//        // declaring an array
//        double[] exampleArray1; // variable type [] array name;
//
//
//        // creating an array and setting size
//        exampleArray1 = new double[10]; // array name = new | variable type | [array size];
//
//
//        // assigning values to an array (exampleArray1)
//        exampleArray1[0] = 5.6; // array name [index] = value;
//        exampleArray1[1] = 4.5;
//        exampleArray1[2] = 3.3;
//        exampleArray1[3] = 13.2;
//        exampleArray1[4] = 4.0;
//        exampleArray1[5] = 34.33;
//        exampleArray1[6] = 34.0;
//        exampleArray1[7] = 45.45;
//        exampleArray1[8] = 99.993;
//        exampleArray1[9] = 11123;
//
//
//        // checking length of the array
//        System.out.println("The length of Array 1 is " + exampleArray1.length + "\n");
//
//
//        // displaying an array
//        System.out.println("Array 1: ");
//        for (int i = 0; i < exampleArray1.length; i++) {
//            System.out.print(exampleArray1[i] + ", ");
//        }
//        System.out.println("\n\n===\n");
//
//        /** Version 2 - declaring and creating in one, no values (defaults)*/
//
//
//        // declaring, creating ans setting size in one
//        double[] exampleArray2 = new double[5]; // variable type [] array name = new | variable type | [array size];
//
//
//        // assigning values to an array (exampleArray2)
//        // no values are assigned - will display default values for the given variable type
//
//
//        // checking length of the array
//        System.out.println("The length of Array 2 is " + exampleArray2.length + "\n");
//
//
//        // displaying an array
//        System.out.println("Array 2: ");
//        for (int i = 0; i < exampleArray2.length; i++) {
//            System.out.print(exampleArray2[i] + ", ");
//        }
//        System.out.println("\n\n===\n");
//
//
//        /** Version 3 - declaring, creating and assigning values in one*/
//
//
//        // declaring, creating ans setting size and assigning values in one
//        double[] exampleArray3 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
//
//
//        // checking length of the array
//        System.out.println("The length of Array 3 is " + exampleArray3.length + "\n");
//
//
//        // displaying an array
//        System.out.println("Array 3: ");
//        for (int i = 0; i < exampleArray3.length; i++) {
//            System.out.print(exampleArray3[i] + ", ");
//        }
//        System.out.println("\n\n===\n");
//
//
//        /** Processing arrays */
//
//
//        /** Initializing arrays with input values from the user */
//
//
//        // declaring, creating ans setting size in one
//        double[] exampleArray4 = new double[5]; // variable type [] array name = new | variable type | [array size];
//
//
//        // checking length of the array
//        System.out.println("The length of Array 4 is " + exampleArray4.length + "\n");
//
//
//        // assigning values in an array with user input values
//        Scanner scan = new Scanner(System.in);
//        System.out.print("Enter " + exampleArray4.length + " values: (enter the highest value twice!)");
//        for (int i = 0; i < exampleArray4.length; i++) {
//            exampleArray4[i] = scan.nextDouble();
//        }
//
//
//        // displaying an array
//        System.out.println("Array 4: ");
//        for (int i = 0; i < exampleArray4.length; i++) {
//            System.out.print(exampleArray4[i] + ", ");
//        }
//        System.out.println("\n\n===\n");
//
//
//        /** Initializing arrays with random values */
//
//
//        // declaring, creating ans setting size in one
//        double[] exampleArray5 = new double[5]; // variable type [] array name = new | variable type | [array size];
//
//
//        // checking length of the array
//        System.out.println("The length of Array 5 is " + exampleArray5.length + "\n");
//
//
//        // assigning random values to an array
//        for (int i = 0; i < exampleArray5.length; i++) {
//            exampleArray5[i] = Math.random() * 100;
//        }
//
//
//        // displaying an array
//        System.out.println("Array 5: ");
//        for (int i = 0; i < exampleArray5.length; i++) {
//            System.out.print(exampleArray5[i] + ", ");
//        }
//        System.out.println("\n\n===\n");
//
//
//        /** Summing all values of an array */
//
//
//        double array4Total = 0;
//        for (int i = 0; i < exampleArray4.length; i++) {
//            array4Total += exampleArray4[i];
//        }
//
//
//        System.out.println("The sum of all numbers in array 4 is " + array4Total);
//        System.out.println("\n\n===\n");
//
//
//        /** Finding the largest element */
//
//
//        double array4Max = exampleArray4[0];
//        for (int i = 1; i < exampleArray4.length; i++) {
//            if (exampleArray4[i] > array4Max) array4Max = exampleArray4[i];
//        }
//
//
//        System.out.println("The largest element in array 4 is " + array4Max);
//        System.out.println("\n\n===\n");
//
//
//        /** Finding the smallest index of the largest element (first largest element in the array)*/
//
//
//        double array4FirstMax = exampleArray4[0];
//        int indexOfFirstMax = 0;
//        for (int i = 1; i < exampleArray4.length; i++) {
//            if (exampleArray4[i] > array4FirstMax) {
//                array4FirstMax = exampleArray4[i];
//                indexOfFirstMax = i;
//            }
//        }
//
//
//        System.out.println("The smallest index of the largest element in array 4 is " + indexOfFirstMax);
//        System.out.println("\n\n===\n");
//
//
//        /** Random shuffling */
//
//
//        // displaying an array before shuffling
//        System.out.println("Array 4 before shuffling: ");
//        for (int i = 0; i < exampleArray4.length; i++) {
//            System.out.print(exampleArray4[i] + ", ");
//        }
//        System.out.println("\n\n===\n");
//
//
//        for (int i = (exampleArray4.length-1); i>0; i--) {
//            int j = (int)(Math.random() * (i + 1));
//            double temp = exampleArray4[i];
//            exampleArray4[i] = exampleArray4[j];
//            exampleArray4[j] = temp;
//        }
//
//        // displaying an array after shuffling
//        System.out.println("Array 4 after shuffling: ");
//        for (int i = 0; i < exampleArray4.length; i++) {
//            System.out.print(exampleArray4[i] + ", ");
//        }
//        System.out.println("\n\n===\n");
//
//
//        /** Shifting elements */
//
//
//        double temp = exampleArray4[0];
//        for (int i = 1; i < exampleArray4.length; i++) {
//            exampleArray4[i - 1] = exampleArray4[i];
//        }
//        exampleArray4[exampleArray4.length - 1] = temp;
//
//
//        // displaying an array after shifting the first value to the last index
//        System.out.println("Array 4 after shifting the first value to the last index: ");
//        for (int i = 0; i < exampleArray4.length; i++) {
//            System.out.print(exampleArray4[i] + ", ");
//        }
//        System.out.println("\n\n===\n");
//
//
//        /** Shifting elements */
//
//
//        String[] months = {"January", "February", "March", "April", "May", "June", "July", "August",
//                "September", "October", "November", "December"};
//        System.out.print("Enter a month number (1 to 12): ");
//        int monthNumber = scan.nextInt();
//        System.out.println("The month is " + months[monthNumber - 1]);
//
//
//        /** Copying an array */
//
//        // create: an array with 9 values, an array with 9 empty spaces, an array with 15 empty spaces
//        double[] arrayBeforeCopying = {1, 2, 3, 4, 5, 6, 7, 8, 9};
//        double[] arrayAfterCopying9 = new double[9];
//        double[] arrayAfterCopying15 = new double[15];
//
//        // create a loop that copies arrayBeforeCopying into both other arrays index by index
//        for (int i = 0; i < arrayBeforeCopying.length; i++){
//            arrayAfterCopying9[i] = arrayBeforeCopying[i];
//            arrayAfterCopying15[i] = arrayBeforeCopying[i];
//        }
//
//        // use the System.arraycopy method: (source name, starting point, target name, starting point, how much to copy)
//        System.arraycopy(arrayBeforeCopying, 0, arrayAfterCopying9, 0, arrayBeforeCopying.length);
//        System.arraycopy(arrayBeforeCopying, 0, arrayAfterCopying15, 0, arrayBeforeCopying.length);
//
//
//        // print the first 9 items of all arrays (the remaining indexes in arrayAfterCopying15 are default (0.0))
//        for (int i = 0; i < arrayBeforeCopying.length; i++){
//            System.out.println(arrayBeforeCopying[i] + "     " + arrayAfterCopying9[i] + "     " + arrayAfterCopying15[i]);
//        }

//        // create an array with 6 elements
//        int[] list1 = {1, 2, 3, 4, 5, 6};
//
//        // reverse the previous array using a method (below main)
//        int[] list2 = reverse(list1);
//
//        // print the two arrays above
//        for (int i = 0; i < list1.length; i++){
//            System.out.println(list1[i] + "     " + list2[i]);
//        }

//        // Sorting the array using the Arrays class
//        double[] numbers = {6.0, 4.4, 1.9, 2.9, 3.4, 3.5};
//        System.out.println(Arrays.toString(numbers));
//        java.util.Arrays.sort(numbers); // Sort the whole array
//        java.util.Arrays.parallelSort(numbers); // Sort the whole array
//        System.out.println(Arrays.toString(numbers));
//
//        // Sorting the array using the Arrays class
//        char[] chars = {'a', 'A', '4', 'F', 'D', 'P'};
//        System.out.println(Arrays.toString(chars));
//        java.util.Arrays.sort(chars, 1, 3); // Sort part of the array
//        java.util.Arrays.parallelSort(chars, 1, 3); // Sort part of the array
//        System.out.println(Arrays.toString(chars));

//        // searching for a key
//        int[] list = {2, 4, 7, 10, 11, 45, 50, 59, 60, 66, 69, 70, 79};
//        System.out.println("1. Index is " +
//                java.util.Arrays.binarySearch(list, 11));
//        System.out.println("2. Index is " +
//                java.util.Arrays.binarySearch(list, 12));
//
//        char[] chars = {'a', 'c', 'g', 'x', 'y', 'z'};
//        System.out.println("3. Index is " +
//                java.util.Arrays.binarySearch(chars, 'a'));
//        System.out.println("4. Index is " +
//                java.util.Arrays.binarySearch(chars, 't'));

//        // Comparing Arrays // must be in the same order
//        int[] list1 = {2, 4, 7, 10};
//        int[] list2 = {2, 4, 7, 10};
//        int[] list3 = {4, 2, 7, 10};
//        System.out.println(java.util.Arrays.equals(list1, list2)); // true
//        System.out.println(java.util.Arrays.equals(list2, list3)); // false

//        // filling Arrays
//        int[] list1 = {2, 4, 7, 10};
//        int[] list2 = {2, 4, 7, 7, 7, 10};
//        java.util.Arrays.fill(list1, 5); // Fill 5 to the whole array
//        java.util.Arrays.fill(list2, 1, 5, 8); // Fill 8 to a partial array
//        System.out.println(Arrays.toString(list1));
//        System.out.println(Arrays.toString(list2));
    }

    // method reversing the indexes of the array
    public static int[] reverse(int[] list) {
        int[] result = new int[list.length];

        for (int i = 0, j = result.length - 1; i < list.length; i++, j--) {
            result[j] = list[i];
        }

        return result;
    }
}
