package introductionToJavaProgramming.unit_7_Arrays;

import introductionToJavaProgramming.unit_6_Methods.Task_6_0_10_RandomCharacter;

import java.util.Random;

public class Task_7_0_4_CountLettersInArray {
    public static void main(String[] args) {

        // declare and create an array
        char[] chars = createArray();


        // display the array
        System.out.println("The lowercase letters are: ");
        displayArray(chars);


        // count the occurrences of each letter
        int[] counts = countLetters(chars);


        // Display counts
        System.out.println();
        System.out.println("The occurrences of each letter are: ");
        displayCounts(counts);
    }

    // Create an array of characters
    public static char[] createArray() {
        // Declare and create a character
        char[] chars = new char[100];

        // Create lowercase letters and randomly assign them to the array
        for (int i = 0; i < chars.length; i++) {
            chars[i] = Task_6_0_10_RandomCharacter.getRandomLowerCaseLetter();
        }

        return chars;
    }

    // Display the array of characters
    public static void displayArray(char[] chars) {
        // Display characters in the array 20 at a time
        for (int i = 0; i < chars.length; i++) {
            if ((i + 1) % 20 == 0) {
                System.out.println(chars[i]);
            } else {
                System.out.print(chars[i] + " ");
            }
        }
    }

    // Count occurrences of each letter
    public static int[] countLetters(char[] chars) {
        // Declare and create an array of 26 int
        int[] counts = new int[26];

        // For each lowercase letter in the array, count it
        for (int i = 0; i < chars.length; i++) {
            counts[chars[i] - 'a']++;
        }

        return counts;
    }

    // Display counts
    public static void displayCounts(int[] counts) {
        for (int i = 0; i < counts.length; i++) {
            if ((i + 1) % 10 == 0) {
                System.out.println(counts[i] + " " + (char) (i + 'a'));
            } else {
                System.out.print(counts[i] + " " + (char) (i + 'a') + ", ");
            }
        }
    }

}
