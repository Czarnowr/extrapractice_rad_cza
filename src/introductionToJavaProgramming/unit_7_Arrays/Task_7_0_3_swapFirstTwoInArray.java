package introductionToJavaProgramming.unit_7_Arrays;

public class Task_7_0_3_swapFirstTwoInArray {
    public static void main(String[] args) {

        /*
        Listing 7.3, page 260

        The program contains two methods for swapping elements in an array. The first method, named swap, fails to
        swap two int arguments. The second method, named swapFirst- TwoInArray, successfully swaps the first two
        elements in the array argument.
         */

        int[] a = {1, 2};

        // swap elements using the swap method
        System.out.println("Before invoking swap method is {" + a[0] + ", " + a[1] + "}");
        swap(a[0], a[1]);
        System.out.println("After invoking swap method is {" + a[0] + ", " + a[1] + "} ");

        // swap elements using the swapFirstTwoInArray method
        System.out.println("Before invoking swapFirstTwoInArray method is {" + a[0] + ", " + a[1] + "}");
        swapFirstTwoInArray(a);
        System.out.println("After invoking swapFirstTwoInArray method is {" + a[0] + ", " + a[1] + "}");
    }

    // Swap two variables
    public static void swap(int n1, int n2) {
        int temp = n1;
        n1 = n2;
        n2 = temp;
    }

    public static void swapFirstTwoInArray(int[] array) {
        int temp = array[0];
        array[0] = array[1];
        array[1] = temp;
    }

}
