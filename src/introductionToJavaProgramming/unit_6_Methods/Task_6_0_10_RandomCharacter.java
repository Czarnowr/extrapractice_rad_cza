package introductionToJavaProgramming.unit_6_Methods;

public class Task_6_0_10_RandomCharacter {

        /*
        Listing 2.7, page 52

        Listing 6.10 defines a class named RandomCharacter with five overloaded methods to get a certain type of
        character randomly.
         */

    //Generate a random character between ch1 and ch2
    public static char getRandomCharacter(char ch1, char ch2) {
        return ( char)(ch1 + Math.random() * (ch2-ch1+1));
    }

    // Get a random lowercase letter
    public static char getRandomLowerCaseLetter(){
        return getRandomCharacter('a', 'z');
    }

    // Get a random uppercase letter
    public static char getRandomUpperCaseLetter(){
        return getRandomCharacter('A', 'Z');
    }

    // Generate a random digit character
    public static char getRandomDigitCharacter(){
        return getRandomCharacter('0', '9');
    }

    // Generate a random character
    public static char getRandomCharacter(){
        return getRandomCharacter('\u0000', '\uFFFF');
    }
}
