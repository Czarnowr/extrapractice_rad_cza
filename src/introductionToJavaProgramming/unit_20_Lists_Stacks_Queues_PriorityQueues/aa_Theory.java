package introductionToJavaProgramming.unit_20_Lists_Stacks_Queues_PriorityQueues;

import java.util.*;

public class aa_Theory {
    public static void main(String[] args) {

//        // sorting in ascending order
//        List<String> list1 = Arrays.asList("red", "green", "blue");
//        System.out.println("Before sorting: ");
//        System.out.println(list1);
//        Collections.sort(list1);
//        System.out.println("After sorting: ");
//        System.out.println(list1);

//        // sorting in descending order using a Comparator
//        List<String> list2 = Arrays.asList("blue", "green", "red", "yellow");
//        System.out.println("Before sorting: ");
//        System.out.println(list2);
//        Collections.sort(list2, Collections.reverseOrder());
//        System.out.println("After sorting: ");
//        System.out.println(list2);

//        // using binarySearch to search for a key
//        List<Integer> list3 = Arrays.asList(2, 4, 7, 10, 11, 45, 50, 59, 60, 66);
//        System.out.println("(1)Index: " + Collections.binarySearch(list3, 7));
//        System.out.println("(2)Index: " + Collections.binarySearch(list3, 9));
//
//        List<String> list4 = Arrays.asList("blue", "green", "red");
//        System.out.println("(3)Index: " + Collections.binarySearch(list4, "red"));
//        System.out.println("(4)Index: " + Collections.binarySearch(list4, "cyan"));

//        // using the reverse method
//        List<String> list5 = Arrays.asList("yellow", "red", "green", "blue");
//        System.out.println("Before sorting: ");
//        System.out.println(list5);
//        Collections.reverse(list5);
//        System.out.println("After sorting: ");
//        System.out.println(list5);

//        // using the shuffle method
//        List<String> list6 = Arrays.asList("yellow", "red", "green", "blue");
//        System.out.println("Before sorting: ");
//        System.out.println(list6);
//        Collections.shuffle(list6);
//        System.out.println("After sorting 1 time: ");
//        System.out.println(list6);
//        Collections.shuffle(list6);
//        System.out.println("After sorting 2 times: ");
//        System.out.println(list6);
//        Collections.shuffle(list6);
//        System.out.println("After sorting 3 times: ");
//        System.out.println(list6);

//        // using the shuffle method with a 'seed'
//        List<String> list7 = Arrays.asList("yellow", "red", "green", "blue");
//        List<String> list8 = Arrays.asList("yellow", "red", "green", "blue");
//        System.out.println("Before sorting: ");
//        System.out.println(list7);
//        System.out.println(list8);
//        Collections.shuffle(list7, new Random(5));
//        Collections.shuffle(list8, new Random(5));
//        System.out.println("After sorting with '5': ");
//        System.out.println(list7);
//        System.out.println(list8);
//        Collections.shuffle(list7, new Random(10));
//        Collections.shuffle(list8, new Random(10));
//        System.out.println("After sorting with '10': ");
//        System.out.println(list7);
//        System.out.println(list8);
//        Collections.shuffle(list7, new Random(15));
//        Collections.shuffle(list8, new Random(15));
//        System.out.println("After sorting with '15': ");
//        System.out.println(list7);
//        System.out.println(list8);

//        // using the copy method (destination, source) -> only references are copies
//        List<String> list9 = Arrays.asList("yellow", "red", "green", "blue");
//        List<String> list10 = Arrays.asList("white", "black");
//        System.out.println("Before copying: ");
//        System.out.println(list9);
//        System.out.println(list10);
//        Collections.copy(list9, list10);
//        System.out.println("After copying: ");
//        System.out.println(list9);
//        System.out.println(list10);

//        // using the copy method (n copies) ???
//        List<GregorianCalendar> list11 = Collections.nCopies(5, new GregorianCalendar(2005, 0, 1));
//        System.out.println(list11);

//        // using the fill method
//        List<String> list12 = Arrays.asList("red", "green", "blue");
//        System.out.println("Before: " + list12);
//        Collections.fill(list12, "black");
//        System.out.println("After: " + list12);

//        // using the max and min methods
//        Collection<String> collection = Arrays.asList("red", "green", "blue");
//        System.out.println(collection);
//        System.out.println("Max: " + Collections.max(collection));
//        System.out.println("Min: " + Collections.min(collection));

//        // using the disjoint method (true if no elements in common
//        Collection<String> collection1 = Arrays.asList("red", "cyan");
//        Collection<String> collection2 = Arrays.asList("red", "blue");
//        Collection<String> collection3 = Arrays.asList("pink", "tan");
//        System.out.println("1 and 2: " + Collections.disjoint(collection1, collection2));
//        System.out.println("1 and 3: " + Collections.disjoint(collection1, collection3));

        //using the frequency method
        Collection<String> collection4 = Arrays.asList("red", "cyan", "red");
        System.out.println("No of red: " + Collections.frequency(collection4, "red"));
    }
}
