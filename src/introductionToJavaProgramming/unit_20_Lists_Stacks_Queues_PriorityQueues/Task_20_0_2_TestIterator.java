package introductionToJavaProgramming.unit_20_Lists_Stacks_Queues_PriorityQueues;

import java.util.*;

public class Task_20_0_2_TestIterator {
    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();
        collection.add("New York");
        collection.add("Atlanta");
        collection.add("Dallas");
        collection.add("Madison");

        Iterator<String> iterator = collection.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next().toUpperCase() + " ");
        }
        System.out.println();

        for (String element: collection)
            System.out.print(element.toUpperCase() + " ");
    }
}
