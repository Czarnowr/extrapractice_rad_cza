package introductionToJavaProgramming.unit_20_Lists_Stacks_Queues_PriorityQueues;

import introductionToJavaProgramming.unit_13_AbstractClassesAndInterfaces.Task_13_0_1_GeometricObject;

import java.util.Comparator;

public class Task_20_0_4_GeometricObjectComparator implements Comparator<Task_13_0_1_GeometricObject>, java.io.Serializable {

    public int compare(Task_13_0_1_GeometricObject o1, Task_13_0_1_GeometricObject o2){
     double area1 = o1.getArea();
     double area2 = o2.getArea();

     if(area1 < area2){
         return -1;
     }else if (area1 == area2){
         return 0;
     }else {
         return 1;
     }
 }
}
