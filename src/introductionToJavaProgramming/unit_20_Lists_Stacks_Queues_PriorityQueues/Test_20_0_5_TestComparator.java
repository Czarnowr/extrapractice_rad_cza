package introductionToJavaProgramming.unit_20_Lists_Stacks_Queues_PriorityQueues;

import java.util.Comparator;
import introductionToJavaProgramming.unit_13_AbstractClassesAndInterfaces.Task_13_0_1_GeometricObject;
import introductionToJavaProgramming.unit_13_AbstractClassesAndInterfaces.Task_13_0_2_Circle;
import introductionToJavaProgramming.unit_13_AbstractClassesAndInterfaces.Task_13_0_3_Rectangle;

public class Test_20_0_5_TestComparator {
    public static void main(String[] args) {

        Task_13_0_1_GeometricObject g1 = new Task_13_0_3_Rectangle(5, 5);
        Task_13_0_1_GeometricObject g2 = new Task_13_0_2_Circle(5);

        Task_13_0_1_GeometricObject g = max (g1, g2, new Task_20_0_4_GeometricObjectComparator());

        System.out.println("The area of the larger object is " + g.getArea());
    }

    //TODO: line different than book <Geometric Object> removed as it did not work :(
    public static Task_13_0_1_GeometricObject max (Task_13_0_1_GeometricObject g1, Task_13_0_1_GeometricObject g2, Task_20_0_4_GeometricObjectComparator c){
        if (c.compare(g1, g2) > 0){
            return g1;
        }else {
            return g2;
        }
    }
}
